/**
 * <p>
 * Array Basics
 * </p>
 *
 * We begin by working on some simple exercises to learn the basics of using arrays.
 *
 * <br />
 * <br />
 *
 * Create a project called <b>{@link ArrayBasics#ArrayBasics ArrayBasics}</b> with a single class containing the
 * following code:
 *
 * <pre>
 * public class ArrayBasics {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         int[] myArray = {8, 4, 5, 21, 7, 9, 18, 2, 100};
 *
 *         System.out.println ("Length of myArray: " + myArray.length);
 *         System.out.println ("Value at index 3: " + myArray[3]);
 *         System.out.println ("Value at index 4: " + myArray[4]);
 *
 *         myArray[3] = myArray[4];
 *         System.out.println ("Value at index 3: " + myArray[3]);
 *         System.out.println ("Value at index 4: " + myArray[4]);
 *     }
 * }
 * </pre>
 *
 * What do you think will be displayed when this program is run? Run the program to check if you are right.
 *
 * <br />
 * <br />
 *
 * Add lines of code to display the following:
 *
 * <ul>
 * <li>The first item in the array;</li>
 * <li>The last item in the array;</li>
 * <li>The value of the expression {@code myArray[myArray.length - 1]}. Why is this value the same as the previous
 * value displayed?</li>
 * </ul>
 *
 * Complete the {@code for} loop below which prints all the values in {@code myArray} with labels indicating their
 * position in the array. The first two lines of output could look like this:
 *
 * <pre>
 * index: 0        value: 8
 * index: 1        value: 4
 * </pre>
 *
 * <pre>
 * for(int i = 0; i {@literal <} myArray.length; i++) {
 *     // Your code goes here
 * }
 * </pre>
 *
 * What do you think would be the result of running your program if the {@code for} loop was changed in each of the
 * ways listed below?
 *
 * <ul>
 * <li>The loop variable {@code i} is initialised as {@code 1} instead of {@code 0};</li>
 * <li>The {@literal <} in the loop condition is changed to {@code <=};</li>
 * <li>The update statement is changed to {@code i += 2}?</li>
 * </ul>
 *
 * Try out each change one at a time to see if you are right. Undo each change after you have found out its effect.
 *
 * <br />
 * <br />
 *
 * Displaying the contents of an array is a useful function that you may wish to perform several times in a program. We
 * will put this code inside a method, so that it can be easily reused. Add the method below to your program, using
 * your {@code for} loop code to complete its body. Notice that the method takes a parameter called {@code array}. You
 * will need to modify your code to use this parameter rather than the variable {@code myArray} from the
 * {@link ArrayBasics#main main} method.
 *
 * <pre>
 * {@literal /**}
 *  * List the indices and values held in the given array.
 *  * {@literal @param} array an array of integers.
 *  *&#47;
 * public static void printArray(int[] array) {
 *     // Add your code here
 * }
 * </pre>
 *
 * In the {@link ArrayBasics#main main} method, replace your original {@code for} loop code with a call to
 * {@link ArrayBasics#printArray printArray} with the argument {@code myArray}. Compile and run your program. Does it
 * still work as it did before?
 *
 * <br />
 * <br />
 *
 * Write a method called {@link ArrayBasics#printBackwards printBackwards} that, when passed an array of
 * {@link java.lang.Integer integers}, displays the elements in the array in reverse order, that is, with the last
 * element first and the first element last. Each element should be labelled with its position in the array. Below is a
 * skeleton for your method. You will need to think carefully about how to change the {@code for} loop to cause it to
 * traverse the array in reverse.
 *
 * <pre>
 * {@literal /**}
 *  * List the indices and values in an array in reverse order.
 *  * {@literal @param} array an array of integers.
 *  *&#47;
 * public static void printBackwards(int[] array) {
 *     // Add your code here
 * }
 * </pre>
 *
 * Add a call to {@link ArrayBasics#printBackwards printBackwards} in the {@link ArrayBasics#main main} method, giving
 * it the argument {@code myArray}. Compile and run your program.
 *
 * <br />
 * <br />
 *
 * The method below uses an enhanced {@code for} loop below to calculate the sum of all the elements in
 * {@code myArray}. Add this code to your program. Then add a line to the {@link ArrayBasics#main main} method to print
 * the sum of the values in {@code myArray}. Compile and run your program.
 *
 * <br />
 * <br />
 *
 * Make sure you understand how the enhanced {@code for} loop works. You can read more about it in Section 6.2 of Java
 * for Everyone.
 *
 * <pre>
 * {@literal /**}
 *  * Find the sum of all the values held in an {@link java.lang.Integer integer} array.
 *  * {@literal @param} array an array of integers.
 *  *&#47;
 * public static int sum(int[] array) {
 *     int total = 0;
 *     for (int element : array) {
 *         total = total + element;
 *     }
 *     return total;
 * }
 * </pre>
 *
 * The code below returns an array of a given length filled with randomly chosen {@link java.lang.Integer integers}
 * between the given bounds {@code min} and {@code max}. Read through the code and try to understand how it works. If
 * you are unsure about how the random numbers are calculated, read Section 4.9.2 of Java for Everyone.
 *
 * <pre>
 * {@literal /**}
 *  * Return an array of a given length filled
 *  * with random {@link java.lang.Integer integers} between given bounds
 *  *
 *  * {@literal @param} length the length of the result.
 *  * {@literal @param} min minimum bound for the array elements.
 *  * {@literal @param} max maximum bound for the array elements.
 *  *&#47;
 * public static int[] randomArray(int length, int min, int max) {
 *     int[] array = new int[length];
 *     for(int i = 0; i {@literal <} length; i++) {
 *         array[i] = (int)(Math.random()*(max – min + 1) + min);
 *     }
 *     return array;
 * }
 * </pre>
 *
 * Add this code to your program. Then, in the {@link ArrayBasics#main main} method, add code to replace
 * {@code myArray} with an array of the same length as the initial {@code myArray} but filled with randomly chosen
 * {@link java.lang.Integer integers} between 0 and 100. Add code to list the new contents of {@code myArray}. Compile
 * and run your program.
 *
 * <br />
 * <br />
 *
 * Test your skills by writing methods to carry out the tasks listed below. In each case, a method stub is provided.
 * Test each method by adding suitable code to the {@link ArrayBasics#main main} method. You can use the
 * {@link ArrayBasics#randomArray randomArray()} method to generate arrays to use in your testing.
 *
 * <ol>
 * <li>Find the product of all the values in an array of {@link java.lang.Integer integers};
 *
 * <pre>
 *         {@literal /**}
 *          * Find the product of all the values held in an {@link java.lang.Integer integer} array.
 *          * {@literal @param} array an array of {@link java.lang.Integer integers}.
 *          *&#47;
 *         public static int product(int[] array) {
 *             // Your code goes here
 *         }
 *     </pre>
 *
 * </li>
 * <li>Find the minimum value in an array of {@link java.lang.Integer integers};
 *
 * <pre>
 *     {@literal /**}
 *      * Find the minimum value in an array of {@link java.lang.Integer integers}.
 *      * {@literal @param} array an array of {@link java.lang.Integer integers}.
 *      *&#47;
 *     public static int minimum(int[] array) {
 *         // Your code goes here
 *     }
 *     </pre>
 *
 * </li>
 * <li>Search through an array of {@link java.lang.Integer integers} to see if it contains a given
 * {@link java.lang.Integer integer} and return the index at which it was found (-1 if not found). [There are many ways
 * to do this. A standard, count-controlled {@code for} loop which traverses the whole array is not the most efficient
 * solution, so try to think of a better strategy.]
 *
 * <pre>
 *         {@literal /**}
 *          * Search for a given value in an array of {@link java.lang.Integer integers}.
 *          * {@literal @param} array an array of {@link java.lang.Integer integers}.
 *          * {@literal @param} target the value to search for.
 *          *&#47;
 *         public static int find(int[] array, int target) {
 *             // Your code goes here
 *         }
 *     </pre>
 *
 * </li>
 * </ol>
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */
package arrays.arrays1;
