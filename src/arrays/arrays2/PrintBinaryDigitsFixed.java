package arrays.arrays2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 10 <br />
 * Arrays 2 <br />
 * <code>PrintBinaryDigitsFixed.java</code>
 *
 * <p>
 * Demonstrating how to print out binary digits.
 * </p>
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */

public class PrintBinaryDigitsFixed {
    /**
     * Constructor.
     */
    public PrintBinaryDigitsFixed() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        final int MIN = 0;
        final int MAX = (int) (Math.pow(2, 30) - 1);
        int[] digits = new int[30];     // Array to hold the digits

        int number = readInput("Enter an integer from " + MIN + " to " + MAX, MAX);
        int counter = 0;                // Counter
        int q = number / 2;             // Quotient
        int r = number % 2;             // Remainder (modulus)
        int qr;                         // Returning quotient
        while (number > 0) {
            number = q;
            digits[counter] = r;
            qr = q / 2;                 // Quotient to be returned
            r = q % 2;                  // New remainder (modulus) to be returned
            q = qr;                     // Resetting the number to be divided
            counter++;
        }
        printBackwardsFrom(digits, counter);
    }

    /**
     * Print the contents of an array backwards, starting from the last index.
     * @param array an array of {@link java.lang.Integer integers}.
     * @param last  the index of the starting location.
     */
    private static void printBackwardsFrom(int[] array, int last) {
        for (int i = last - 1; i >= 0; i--) {
            System.out.print(array[i]);
        }
    }

    /**
     * Read in an {@link java.lang.Integer integer} between two given bounds.
     * @param prompt the prompt to show to the user.
     * @param max    the largest acceptable input value.
     * @return The input.
     */
    private static int readInput(String prompt, int max) {
        int input = 0;
        boolean running = false;
        while (!running) {
            input = readInteger(prompt);
            if (input < 0 || input > max) {
                System.out.println("Bad input");
            } else {
                running = true;
            }
        }
        return input;
    }

    /**
     * Read in a positive {@link java.lang.Integer integer} and return its value.
     * @param prompt the prompt to be shown to the user.
     * @return The {@link java.lang.Integer integer}.
     */
    private static int readInteger(String prompt) {
        System.out.print(prompt + ": ");
        try (Scanner scan = new Scanner(System.in)) {
            while (!scan.hasNextInt()) {    // While non-integers are present...
                scan.next();                // ...read and discard then input, then prompt again
                System.out.print("Bad input. Enter an integer: ");
            }
            return scan.nextInt();
        }
    }
}
