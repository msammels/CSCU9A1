/**
 * <p>
 * Working with Partially Filled Arrays
 * </p>
 *
 * In this part of the practical you will see how to use an array to solve a problem we encountered in
 * {@link loops.loopsA Practical 7}. This will also give you some practice in programming with partially filled arrays.
 * Read Section 6.1.3 of Java for Everyone before proceeding with this part.
 *
 * <br />
 * <br />
 *
 * {@link loops.loopsA.loopsA2.PrintBinaryDigits Task 5} in Practical 7 involved writing a program which read in a
 * positive decimal {@link java.lang.Integer integer} and then printed the digits of the binary equivalent of that
 * {@link java.lang.Integer integer}. The program calculated the binary digits by repeatedly dividing the
 * {@link java.lang.Integer integer} by 2 and taking the remainder. Each digit was printed as soon as it was
 * calculated.
 *
 * <br />
 * <br />
 *
 * The problem with this was that the program calculates the digits in the reverse order to the order in which they
 * should be displayed on the screen. Therefore, if the user provides the input {@code 13}, say, the program displays
 * <b>1011 (= 1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup> + 1 x 2<sup>3</sup>)</b> when the correct
 * result should be <b>1101 (= 1 x 2<sup>3</sup> + 1 x 2<sup>2</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>0</sup>)</b>.
 *
 * <br />
 * <br />
 *
 * We will solve this problem by using an array to store the binary digits as they are generated. When all of the
 * digits have been stored, we will print off the contents of the array in reverse order. The digits will then appear
 * correctly on the screen.
 *
 * <br />
 * <br />
 *
 * There is a slight hitch. How large should the array be? The number of binary digits is not fixed but depends upon
 * the input the user types in. The larger the input, the more binary digits generated. Therefore we do not know in
 * advance how many digits the array will need to store.
 *
 * <br />
 * <br />
 *
 * The solution is to use an array that is large enough to hold the maximum number of digits we are willing to allow.
 * Then, when we run the program with a given input, we will <i>partially</i> fill this array with the binary digits
 * generated from the input. A <i>companion variable</i> will be used to keep track of how much of the array has been
 * used. Only the values in the filled portion of the array will be printed out when the digits are displayed.
 *
 * <br />
 * <br />
 *
 * We will use an array containing 30 elements. This is large enough to hold binary digits for numbers from 0 up to 230
 * – 1 (that is, 1,073,741,823), which is more than enough for our purposes.
 *
 * <br />
 * <br />
 *
 * Enough explanation, let’s start coding!
 *
 * <br />
 * <br />
 *
 * Create a new project called <b>{@link PrintBinaryDigitsFixed#PrintBinaryDigitsFixed PrintBinaryDigitsFixed}</b> with
 * a single class containing the partially complete code below. Then write your own code to fill in the incomplete
 * sections. There are four parts to be completed: the {@link PrintBinaryDigitsFixed#main main} method; the
 * {@link PrintBinaryDigitsFixed#printBackwardsFrom printBackwardsFrom} method; the
 * {@link PrintBinaryDigitsFixed#readInput readInput} method; and the {@link PrintBinaryDigitsFixed#readInteger
 * readInteger} method. The last three of these are very similar to methods that you have already written for previous
 * practicals, so you will be able to reuse this code with suitable adjustments. The comments in the code give more
 * detailed guidance.
 *
 * <pre>
 * import java.util.Scanner;
 *
 * {@literal /**}
 *  * A program that prints the binary digits of a positive {@link java.lang.Integer integer}.
 *  * {@literal @author} Your Name
 *  * {@literal @version} Today's Date
 *  *&#47;
 *
 * public class PrintBinaryFixed {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         final int MIN = 0;
 *         final int MAX = (int) (Math.pow(2,30)-1);
 *         int[] digits = new int[30]; // array to hold the digits
 *         int number = readInput("Enter an integer from " + MIN + " to " + MAX, MIN, MAX);
 *
 *         // Your code goes here. This code must do the following:
 *         // Declare a "companion" variable to count how many digits stored
 *         // Initialise that variable appropriately
 *         //
 *         // While number > 0
 *         //     Store the remainder (number % 2) in the array
 *         //     Update the companion variable
 *         //     Set number to be number / 2
 *         // Display the filled portion of the array in reverse order
 *     }
 *
 *     {@literal /**}
 *      * Print the contents of an array backwards, starting from the last index.
 *      * {@literal @param} array an array of {@link java.lang.Integer integers}.
 *      * {@literal @param} last the index of the starting location.
 *      *&#47;
 *     public static void printBackwardsFrom(int[] array, int last) {
 *         // Your code goes here
 *         // This code is very similar, but not quite identical to the
 *         // printBackwards method from Part 1 of this practical sheet.
 *         // Print all the digits on the same line with no spaces
 *         // between them.
 *     }
 *
 *     {@literal /**}
 *      * Read in an {@link java.lang.Integer integer} between two given bounds.
 *      * {@literal @param} prompt the prompt to show the user.
 *      * {@literal @param} min the smallest acceptable input value.
 *      * {@literal @param} max the largest acceptable input value.
 *      *&#47;
 *     public static int readInput(String prompt, int min, int max) {
 *         // Your code goes here.
 *         // This method is identical to the readInteger method
 *         // in the InteractiveRectangle project in Part 2 of
 *         // Practical 9.
 *     }
 *
 *     {@literal /**}
 *      * Read in a positive {@link java.lang.Integer integer} and return its value.
 *      * {@literal @param} prompt the prompt to be shown to the user.
 *      *&#47;
 *     public static int readInteger(String prompt) {
 *         // Your code goes here.
 *         // This method is very similar, but not identical to the
 *         // readPositiveInteger method in the InteractiveRectangle project
 *         // in Part 2 of Practical 9.
 *     }
 * </pre>
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */
package arrays.arrays2;
