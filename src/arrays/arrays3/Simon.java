package arrays.arrays3;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 10 <br />
 * Arrays 3 <br />
 * <code>Simon.java</code>
 *
 * <p>
 * A version of the game "Simon".
 * </p>
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */

public class Simon {
    /**
     * This is how we can grab the users input.
     */
    private static Scanner scan;

    /**
     * Constructor.
     */
    public Simon() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        int score = 1;
        boolean lost = false;
        int[] sequence = createSequence();  // Create an array with random digits in the range of 1 to 10

        System.out.println("Welcome to the game of Simon!");
        while (!lost) {
            System.out.println("Memorise these numbers:");
            printSequence(sequence, score);

            // Sleep for 3 seconds
            delay();
            clearScreen();
            if (!readSequence(score, sequence)) {
                lost = true;
                System.out.println("Wrong!");
                System.out.println("Your score is " + score);
            } else {
                System.out.println("Correct!");
                score++;
            }
        }
    }

    /**
     * Clear the console by printing a form feed character.
     */
    private static void clearScreen() {
//        System.out.print('\u000C');

        /*
         * Workaround for non-BlueJ IDEs. Either comment out this, or the command above, depending on the IDE you are
         * currently using
         */
        for (int i = 0; i < 1000; i++) {
            System.out.println();
        }
    }

    /**
     * Creates a randomised sequence of 10 numbers ranging from 1 to 10.
     * @return The sequence as an array.
     */
    private static int[] createSequence() {
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            array[i] = (int) (Math.random() * (9 + 1) + 0);
        }
        return array;
    }

    /**
     * Delay the execution of the program, for a given amount of time.
     */
    private static void delay() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.err.println("Sleep interrupted");
        }
    }

    /**
     * Prints the sequence to be memorised.
     * @param array generated through {@link #createSequence}.
     * @param score handled in {@link #main}.
     */
    private static void printSequence(int[] array, int score) {
        for (int i = 0; i < score; i++) {
            System.out.print(array[i] + " ");
        }
    }

    /**
     * Reads in user input each round and compares it to the provided sequence.
     * @param score the sequence.
     * @param array determines the amount of inputs needed and also the amount to compare with.
     * @return True if correct, otherwise false.
     */
    private static boolean readSequence(int score, int[] array) {
        scan = new Scanner(System.in);
        boolean correct = false;            // True if the user matches the sequence
        int correctCount = 0;               // Compares amount of correct inputs to the amount needed
        int[] userInputs = new int[score];  // Array containing user inputs to be compared to the sequence

        System.out.println("Type in the numbers you saw. Use spaces to separate them:");
        for (int i = 0; i < score; i++) {
            if (scan.hasNext()) {
                int input = scan.nextInt();
                userInputs[i] = input;
            } else {
                return false;
            }
        }
        // Compares user input to the sequence
        for (int j = 0; j < score; j++) {
            if (userInputs[j] == array[j]) {
                correctCount++;
            }
        }
        // Sequence is only correct if the counter is equal to the amount of inputs required
        if (correctCount == score) {
            correct = true;
        }
        return correct;
    }
}
