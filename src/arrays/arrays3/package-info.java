/**
 * <p>
 * A Memory Game
 * </p>
 *
 * For the third practical we have set you a challenge. Use all of your new Java skills to write a program that lets
 * the user play the game of <b>Simon</b>. This is a simple game of memorising and repeating sequences, which you have
 * probably played before.
 *
 * <br />
 * <br />
 *
 * In our version of the Simon game, the player is shown a randomly chosen sequence of numbers from 1 to 10, separated
 * by a single space. Initially, the length of the sequence is 1 (i.e., the player is shown a single number). The
 * sequence is displayed for some time and then disappears from the screen. The player must then try to type in the
 * numbers that were seen. If the player reproduces the sequence correctly, the length of the sequence is increased by
 * one and the game continues for another round. If the player makes a mistake, the game ends and the player’s score is
 * shown. The score is the length of the longest sequence that the player was able to copy accurately.
 *
 * <br />
 * <br />
 *
 * A session with your program might look something like this:
 *
 * <pre>
 * Welcome to the game of Simon!
 *
 * Memorise these numbers:
 * 5
 * [number is shown for some time and then screen is cleared]
 *
 * Type in the numbers you saw. Use spaces to separate them.
 * [User type 5]
 *
 * Correct!
 * Memorise these numbers:
 * 8 3
 * [numbers are shown for some time and then screen is cleared]
 *
 * [User types 8 3]
 * Correct!
 * Memorise these numbers:
 *
 * 1 1 9
 * [numbers are shown for some time and then screen is cleared]
 *
 * [User types 1 2 9]
 * Wrong!
 * Your score is 2
 * </pre>
 *
 * You will need a method which causes the program to wait for some time while the user memorises the sequence. The
 * {@link Simon#delay delay()} method below will cause a delay of {@code length} milliseconds. Experiment to find the
 * best delay length for your game, and consider using longer delays as the sequence to memorise becomes longer. (Note:
 * the {@link Simon#delay delay()} method uses parts of Java which you have not learned about yet (the
 * {@link java.lang.Thread Thread} class and exceptions). You will learn about these in later modules. You do not need
 * to understand them in order to use the method or write the rest of the program.)
 *
 * <pre>
 * {@literal /**}
 *  * Delay execution of program for a given time.
 *  * {@literal @param} length duration of delay in milliseconds.
 *  *&#47;
 * public static void delay(int length) {
 *     try {
 *         Thread.sleep(length);
 *     } catch(InterruptedException e) {
 *         System.out.println("Sleep interrupted!");
 *     }
 * }
 * </pre>
 *
 * You will also need a way to clear the screen after the user has had a chance to memorise the sequence. The
 * {@link Simon#clearScreen clearScreen()} method below works with the BlueJ terminal (though it is not guaranteed to
 * work correctly on other platforms).
 *
 * <pre>
 * {@literal /**}
 *  * Clear the console by printing a form feed character.
 *  *&#47;
 * public static void clearScreen() {
 *     System.out.print('\u000C');
 * }
 * </pre>
 *
 * [Hint: Do not jump straight into coding! This is a challenging problem and you are much more likely to succeed if
 * you design your program on paper first using pseudocode.]
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */
package arrays.arrays3;
