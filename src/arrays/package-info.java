/**
 * <p>
 * <b>Practical 10: Arrays</b>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn how to use arrays for storing and manipulating multiple values.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */
package arrays;
