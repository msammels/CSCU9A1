package data.dataA.dataA1;

/**
 * CSCU9A1 - Practical 3 <br />
 * Data A1 <br />
 * <code>MathsDemo.java</code>
 *
 * <p>
 * Program to practice using numbers and arithmetic.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class MathsDemo {
    /**
     * Constructor.
     */
    public MathsDemo() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        int a = 3;
//        int b = 4;
//        int c = 5;
//        int d = 17;
//        double w = 3;
//        double x = 4;

        System.out.println(a); /** Change this line **/
    }
}
