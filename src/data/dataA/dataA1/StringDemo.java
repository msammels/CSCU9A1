package data.dataA.dataA1;

/**
 * CSCU9A1 - Practical 3 <br />
 * Data A1 <br />
 * <code>StringDemo.java</code>
 *
 * <p>
 * Program to practice using strings.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class StringDemo {
    /**
     * Constructor.
     */
    public StringDemo() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        String s = "Stirling Castle";
//        String t = "The Wallace Monument";

        System.out.println(s); /*** Change this line ***/
    }
}
