/**
 * <p>
 * Variables and Numbers
 * </p>
 *
 * To get you into good programming habits, all the programs you write from now on will include <i>comments</i>. These
 * are explanations that are ignored by Java but are intended for human readers of your code. These readers include
 * you. You would be amazed at how difficult it can be to understand a program you wrote yourself when you come back to
 * it 6 months later, if you did not include any comments. (I speak from bitter experience!)
 *
 * <br />
 * <br />
 *
 * Short comments are written starting with {@code //}. Longer comments are enclosed between the delimiters
 * {@literal /*} and *&#47;. By convention, program files begin with a long explanatory comment starting with the
 * delimiter {@literal /**} and including special {@literal @author} and {@literal @version} tags with information
 * about the author of the program and the date it was written.
 *
 * <br />
 * <br />
 *
 * We begin with some practice declaring and using {@link java.lang.Integer integer} variables and constants.
 *
 * <br />
 * <br />
 *
 * Create a new BlueJ project called <b>{@link IntegerDemo#IntegerDemo IntegerDemo}</b>. Add a class called
 * <b>{@link IntegerDemo#IntegerDemo IntegerDemo}</b> to the project and replace the code in this class with the
 * program below. Change the comment to show your name and today’s date.
 *
 * <br />
 * <br />
 *
 * [<b>Hint</b>: if you are looking at this practical via the Javadoc, you can save time by copy and pasting the code
 * into the BlueJ editor. This may mess up the formatting and indentation of the code. You can tidy it up quickly by
 * choosing <b>Auto-layout</b> from the editor’s <b>Edit</b> menu. Never leave your code in a mess!]
 *
 * <pre>
 * {@literal /**}
 *  * Program to practice using {@link java.lang.Integer integer} variables
 *  * {@literal @author} Your Name
 *  * {@literal @version} Today's Date
 *  *&#47;
 *
 * public class IntegerDemo {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         int a;       // Declaration of an integer variable
 *         int b = 23;  // Declaration with assignment of initial value
 *         int c, d, e; // Three integer variables declared together
 *
 *         b = b + 30;  // Assignment statement
 *         /****** Add your code after this line ******&#47;
 *     }
 * }
 * </pre>
 *
 * Compile the program to check that there are no syntax errors. Run it if you want. It does not do anything
 * interesting yet.
 *
 * <br />
 * <br />
 *
 * Next, make changes to the program to achieve the following. Compile your program after each change to check for
 * syntax errors:
 *
 * <ul>
 * <li>Change the line declaring {@code a} so that {@code a} is given the initial value {@code 10}.</li>
 * <li>Add a new line of code which assigns {@code c} the value {@code 65}.</li>
 * <li>Add another line which assigns {@code d} the value {@code a + b}.</li>
 * </ul>
 *
 * Now let’s add code to let us see the values held in the variables. Add the following line at the end of the
 * {@link IntegerDemo#main main} method (after the assignment statements you just added).
 *
 * <pre>
 * System.out.println(a);
 * </pre>
 *
 * Compile and run the program. Is the output what you expect to see?
 *
 * <br />
 * <br />
 *
 * The output is correct but not very informative: it shows us a number but does not say what that number is. Let’s
 * make it more user-friendly. Change the line you just added to the following:
 *
 * <pre>
 * System.out.println("a is " + a);
 * </pre>
 *
 * Compile and run the program.
 *
 * <br />
 * <br />
 *
 * The code you just added contains the symbol {@code +}. We saw in the last practical that {@code +} can be used to
 * concatenate {@link java.lang.String strings} or to add numbers. What do you think the {@code +} is being used for in
 * this code?
 *
 * <br />
 * <br />
 *
 * Add another line of code to show the contents of the variable{@code b}. Compile and run the program.
 *
 * <br />
 * <br />
 *
 * Next, do the same for variables {@code c}, {@code d} and{@code e}? Do you run into any problems? What do you think
 * is the cause? Find a solution to the problem.
 *
 * <br />
 * <br />
 *
 * When you have fixed the problem, look at the results of running the program. Are they what you expect?
 *
 * <br />
 * <br />
 *
 * Add another line at the end of the {@link IntegerDemo#main main} method (after the {@link java.lang.System#out
 * System.out.println()} statements) to change the value of {@code a} to {@code 15}.
 *
 * <br />
 * <br />
 *
 * Recall that earlier you assigned {@code d} the value {@code a + b}. Now that you have changed the value of
 * {@code a}, do you expect the value of {@code d} to have changed? To check if you are right, add another statement to
 * print the value of {@code d}; this needs to go after the line in which {@code a} was assigned the value {@code 15}.
 * Did the value of {@code d} change?
 *
 * <br />
 * <br />
 *
 * For your final task with the <b>{@link IntegerDemo#IntegerDemo IntegerDemo}</b> project, declare an
 * {@link java.lang.Integer integer} constant called {@code F} by adding the code below just after the declarations at
 * the start of the {@link IntegerDemo#main main} method. (Note that it is conventional in Java to use capital letters
 * and underscores for the names of constants, for example, {@code A_CONSTANT}.)
 *
 * <pre>
 * final int F = 12; // An integer constant
 * </pre>
 *
 * Add a line at the end of the {@link IntegerDemo#main main} method to print the value of {@code F}. Compile and run
 * your program. Then carry out experiments to answer the following questions:
 *
 * <ol>
 * <li>Can you use {@code F} in an expression assigned to another variable? For instance, can you give {@code e} the
 * value {@code a + F}?</li>
 * <li>Can you assign a new value to {@code F}? For instance, can you give {@code F} the value {@code a + e}?</li>
 * <li>What happens if you do not give {@code F} an initial value?</li>
 * </ol>
 *
 * In the next project you will get some practice using mathematical expressions.
 *
 * <br />
 * <br />
 *
 * Create a new BlueJ project called <b>{@link MathsDemo#MathsDemo MathsDemo}</b>. Add a class called
 * <b>{@link MathsDemo#MathsDemo MathsDemo}</b> to the project and replace the code in this class with the code below:
 *
 * <pre>
 * {@literal /**}
 *  * Program to practice using numbers and arithmetic.
 *  * {@literal @author} Your Name
 *  * {@literal @version} Today's Date
 *  *&#47;
 *
 * public class MathsDemo {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         int a = 3;
 *         int b = 4;
 *         int c = 5;
 *         int d = 17;
 *         double w = 3;
 *         double x = 4;
 *
 *         System.out.println(a); /** Change this line **&#47;
 *     }
 * }
 * </pre>
 *
 * The next exercise is similar to the previous one, but using <i>{@link java.lang.String strings}</i> rather than
 * numbers. I recommend you read Section 2.5 of Java for Everyone before trying this exercise.
 *
 * <br />
 * <br />
 *
 * Create a new BlueJ project called <b>{@link StringDemo#StringDemo StringDemo}</b>. Add a class called
 * <b>{@link StringDemo#StringDemo StringDemo}</b> to the project and type in the program below, personalising the
 * comments as appropriate.
 *
 * <pre>
 * {@literal /**}
 *  * Program to practice using {@link java.lang.String strings}.
 *  * {@literal @author} Your Name
 *  * {@literal @version} Today's Date
 *  *&#47;
 *
 * public class StringDemo {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         String s = "Stirling Castle";
 *         String t = "The Wallace Monument";
 *         System.out.println(s);  /*** Change this line ***&#47;
 *     }
 * }
 * </pre>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */
package data.dataA.dataA1;
