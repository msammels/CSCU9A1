package data.dataA.dataA2;

/**
 * CSCU9A1 - Practical 3 <br />
 * Data A2 <br />
 * <code>Rectangle.java</code>
 *
 * <p>
 * This program implements an application that calculates the hypotenuse of a rectangle and displays the results to the
 * standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class Rectangle {
    /**
     * Constructor.
     */
    public Rectangle() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        int length = 10, width = 20, perimeter = length * 2 + width, area = length * width;

        // Calculate the diagonal of the rectangle, using Pythagoras theorem
        double diagonal = Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2));

        System.out.println("Permieter is " + perimeter);
        System.out.println("Area is " + area);
        System.out.println("Diagonal is" + diagonal);
    }
}
