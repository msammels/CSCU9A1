package data.dataA.dataA2;

/**
 * CSCU9A1 - Practical 3 <br />
 * Data A2 <br />
 * <code>StringPractice.java</code>
 *
 * <p>
 * This program implements an application that practices using {@link java.lang.String string} functions and displaying
 * the results to the standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class StringPractice {
    /**
     * Constructor.
     */
    public StringPractice() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        String s = "Humpty Dumpty sat on a wall";

        System.out.println("String s is \"" + s + "\"");
        System.out.println("Length of string is " + s.length() + " characters");
        System.out.println(s.substring(14) + s.substring(0, 13));
    }
}
