/**
 * <p>
 * Practice using Numbers and Strings
 * </p>
 *
 * In this part of the practical you will use your knowledge of variables, numbers and {@link java.lang.String strings}
 * to solve some moderately challenging problems. Consult the lecture notes and textbook if you get stuck.
 *
 * <br />
 * <br />
 *
 * Create a separate new BlueJ project for each task. Make sure that your code is readable and well-commented and the
 * output from your programs is nicely formatted and user-friendly.
 *
 * <p>
 * <b>Task 1 ({@link Rectangle#Rectangle Rectangle})</b>
 * </p>
 *
 * Suppose that you know the length and width of a rectangle. (Make up values for these and store them in appropriately
 * named variables.) Write a program which prints out three lines showing the following:
 *
 * <ul>
 * <li>The <i>perimeter</i> of the rectangle (<i>i.e.,</i> the sum of the two lengths and two widths);</li>
 * <li>The <i>area</i> of the rectangle (<i>i.e.,</i> the length times the width);</li>
 * <li>The length of the diagonal across the rectangle. Use Pythagoras’s theorem, which says that the diagonal
 * {@code d} of a rectangle with length {@code l} and width {@code w} is given by <i>d = &radic; (l<sup>2</sup> +
 * w<sup>2</sup>)</i></li>
 * </ul>
 *
 * <p>
 * <b>Task 2 ({@link StringPractice#StringPractice String practice})</b>
 * </p>
 *
 * Write a program that uses a {@link java.lang.String String} variable {@code s} containing the string
 * {@code Humpty Dumpty sat on a wall}. Your program must print out the following:
 *
 * <ul>
 * <li>A line with text saying {@code String s is} followed by an open quotation mark, then the {@link java.lang.String
 * string} {@code s}, and then a closing quotation mark and full stop, <i>i.e.,</i> the output should look EXACTLY like
 * this:
 *
 * <pre>
 * String s is "Humpty Dumpty sat on a wall."
 * </pre>
 *
 * </li>
 * <li>A line stating the length of {@link java.lang.String string} {@code s};</li>
 * <li>A line containing the second half of {@code s} followed by the first half of {@code s}, <i>i.e.,</i> it should
 * look like {@code sat on a wall.Humpty Dumpty}</li>
 * </ul>
 *
 * [<b>Hint</b>: For the last part, you will need to use the methods {@link java.lang.String#substring s.substring()}
 * and {@link java.lang.String#length s.length()}.]
 *
 * <p>
 * <b>Task 3 ({@link MilkJugs#MilkJugs Milk jugs})</b>
 * </p>
 *
 * Suppose that you have 5.5 litres of milk and you want to store them in milk jugs that can hold up to 0.75 litres
 * each. You want to know how many completely filled jugs you will have. Write a program to calculate this and display
 * the result on the screen.
 *
 * <br />
 * <br />
 *
 * [<b>Hint 1</b>: Use a constant to store the amount of milk that can be held in a jug. Use an appropriate name and
 * type for the constant and add a comment explaining what it means. See section 2.1.5 of the textbook for a similar
 * example.]
 *
 * <br />
 * <br />
 *
 * [<b>Hint 2</b>: The tricky part of this exercise is getting an {@link java.lang.Integer integer} result from a
 * calculation involving combining floating point numbers. You can use a <i>cast</i> to turn the floating point result
 * into an {@link java.lang.Integer integer}. You can read more about them in Section 2.2.5 of Java for Everyone.]
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */
package data.dataA.dataA2;
