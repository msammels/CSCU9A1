/**
 * <p>
 * <b>Practical 3: Variables and Data</b>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn how to create and use variables and constants.</li>
 * <li>To learn how to use different types of members and arithmetic expressions.</li>
 * <li>To practice working with {@link java.lang.String strings}.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */
package data.dataA;
