package data.dataB.dataB1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 4 <br />
 * Data B1 <br />
 * <code>LargerDouble.java</code>
 *
 * <p>
 * Program to find the larger of two {@link java.lang.Double doubles}.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class LargerDouble {
    /**
     * Constructor.
     */
    public LargerDouble() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter two doubles separated by one or more spaces: ");
            double a = scan.nextDouble();
            double b = scan.nextDouble();
            System.out.print("The larger double is " + Math.max(a, b));
        }
    }
}
