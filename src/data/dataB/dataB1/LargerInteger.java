package data.dataB.dataB1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 4 <br />
 * Data B1 <br />
 * <code>LargerInteger.java</code>
 *
 * <p>
 * Program to find the larger of two {@link java.lang.Integer integers}.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class LargerInteger {
    /**
     * Constructor.
     */
    public LargerInteger() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter two integers separated by one or more spaces: ");
            int a = scan.nextInt();
            int b = scan.nextInt();
            System.out.print("The larger integer is: " + Math.max(a, b));
        }
    }
}
