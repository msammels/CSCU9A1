package data.dataB.dataB1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 4 <br />
 * Data B1 <br />
 * <code>Rectangle.java</code>
 *
 * <p>
 * This program implements an application that calculates the hypotenuse of a rectangle and displays the answers to the
 * standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class Rectangle {
    /**
     * Constructor.
     */
    public Rectangle() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter the length and width, separated by a space: ");
            int length = scan.nextInt(), width = scan.nextInt();
            int perimeter = length * 2 + width * 2, area = length * width;

            // Calculate the diagonal of the rectangle using Pythagoras theorem
            double diagonal = Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2));

            // Display to the standard output
            System.out.println("Perimeter is " + perimeter);
            System.out.println("Area is " + area);
            System.out.println("Diagonal is " + diagonal);
        }
    }
}
