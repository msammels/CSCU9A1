package data.dataB.dataB1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 4 <br />
 * Data B1 <br />
 * <code>StringPractice.java</code>
 *
 * <p>
 * This program implements an application that practices using {@link java.lang.String string} functions and displays
 * the results to the standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */

public class StringPractice {
    /**
     * Constructor.
     */
    public StringPractice() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        System.out.println("Please enter a string: ");

        // Declare and initialise new variables
        try (Scanner scan = new Scanner(System.in)) {
            String s = scan.nextLine();

            // Display the output to the user using the standard output
            System.out.println("\nString s is \"" + s + "\"");
            System.out.println("Length of string is " + s.length() + " characters\n");
            System.out.println(s.substring(s.length() / 2) + s.substring(0, s.length() - 1));
        }
    }
}
