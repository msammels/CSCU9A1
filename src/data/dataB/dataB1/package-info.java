/**
 * <p>
 * Reading User Input
 * </p>
 *
 * Programs are much more useful if they can be given input from their users, rather than relying on data that is
 * “hard-coded” within the program code. In this practical you will learn how to make your programs read data that the
 * user types into the terminal. You can read more about this in Section 2.3 of Java for Everyone.
 *
 * <br />
 * <br />
 *
 * We will create a program that prompts the user to enter two {@link java.lang.Integer integers} and then tells the
 * user which {@link java.lang.Integer integer} is the larger one.
 *
 * <br />
 * <br />
 *
 * Create a new BlueJ project called <b>{@link LargerInteger#LargerInteger LargerInteger}</b>. Add a class called
 * <b>{@link LargerInteger#LargerInteger LargerInteger}</b> and replace the code in it with the following:
 *
 * <pre>
 * <mark>import java.util.Scanner;</mark>
 *
 * {@literal /**}
 *  * Program to find the larger of two integers.
 *  * {@literal @author} Your Name
 *  * {@literal @version Today's Date}
 *  *&#47;
 *
 * public class LargerInteger {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         <mark>Scanner scan = new Scanner(System.in);</mark>
 *
 *         System.out.println("Enter two integers separated by one or more spaces");
 *
 *           <mark>int a = scan.nextInt();</mark>
 *           <mark>int b = scan.nextInt();</mark>
 *         System.out.println("The larger integer is " + Math.max(a, b));
 *     }
 * }
 * </pre>
 *
 * Take a look at the lines of code that are highlighted.
 *
 * <br />
 * <br />
 *
 * In order to read from standard input Java needs to use a library package called {@link java.util.Scanner Scanner}.
 * The first highlighted line imports this library into the program so that its facilities are available.
 *
 * <br />
 * <br />
 *
 * The second highlighted line creates a {@link java.util.Scanner Scanner} <i>object</i> called {@code scan}. (You will
 * learn more about objects in Semester 2; for now, just accept that you need this line in order to read keyboard
 * input.)
 *
 * <br />
 * <br />
 *
 * The third and fourth highlighted lines show how the {@link java.util.Scanner Scanner} object can be used to read an
 * {@link java.lang.Integer integer} that is typed in and store that {@link java.lang.Integer integer} in a variable.
 *
 * <br />
 * <br />
 *
 * Run the program a few times, trying out different inputs. Answer the following questions:
 *
 * <ul>
 * <li>What happens if the two {@link java.lang.Integer integers} you enter are identical?</li>
 * <li>What happens if you press return in between typing the two {@link java.lang.Integer integers}?</li>
 * <li>What happens if you type in more than two {@link java.lang.Integer integers}?</li>
 * <li>What happens if you type in something that is not an {@link java.lang.Integer integer}, such as a
 * {@link java.lang.Double double} or some text?</li>
 * </ul>
 *
 * When a Java program encounters a situation it is not equipped to deal with, it <i>throws an exception</i> (or, as I
 * like to think of it, it throws a tantrum!). This is what happened when you typed in something that was not an
 * {@link java.lang.Integer integer}: {@link java.util.Scanner#nextInt scan.nextInt()} encountered whatever you typed,
 * did not know how to deal with it, and threw an exception. This caused the program to crash. In the next practical
 * you will learn how to avoid this particular error by checking that there is an {@link java.lang.Integer integer} in
 * the input before attempting to read one in. Later on, you will learn how to make your programs handle exceptions
 * gracefully so that they do not crash. For now, just be careful to give your programs the kind of input they expect.
 *
 * <br />
 * <br />
 *
 * Your next task is a small challenge. You will adapt the program you just wrote to work with decimal numbers
 * ({@link java.lang.Double doubles}) rather than {@link java.lang.Integer integers}. Use
 * {@link java.util.Scanner#nextDouble scan.nextDouble()} to read a decimal number. (For future reference, to read a
 * {@link java.lang.String string} consisting of a single word terminated by a space, use {@link java.util.Scanner#next
 * scan.next()}; to read a {@link java.lang.String string} consisting of a whole line terminated by a newline, use
 * {@link java.util.Scanner#nextLine scan.nextLine()}.)
 *
 * <br />
 * <br />
 *
 * Create a copy of your last project, giving it the name <b>{@link LargerDouble#LargerDouble LargerDouble}</b>. Make
 * all changes necessary to cause the program to accept two decimal numbers and tell the user which one is larger.
 * There are five changes to be made.
 *
 * <br />
 * <br />
 *
 * Did you manage it? You now have one (or more accurately, three) more tasks to do to complete the this part of the
 * practical.
 *
 * <br />
 * <br />
 *
 * Make copies of the three programs you created for Part 2 of the last practical. Modify each of these so that the
 * relevant input is typed in at the keyboard, instead of being hard- coded into the program. So, the
 * {@link Rectangle#Rectangle rectangle} program should read in the length and width of the rectangle, the
 * {@link StringPractice#StringPractice strings} program should read in the {@link java.lang.String string} to store in
 * {@code s}, and the {@link MilkJugs#MilkJugs milk jugs} program should read in how much milk you have. Add
 * appropriate prompts to let the user know what to type in.
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */
package data.dataB.dataB1;
