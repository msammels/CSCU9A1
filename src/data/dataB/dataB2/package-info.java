/**
 * <p>
 * Solving a Larger Problem
 * </p>
 *
 * Part two of this sheet is a longer, more open-ended exercise which lets you put your new skills into practice. It is
 * based on programming exercise P2.10 from Java for Everyone.
 *
 * <br />
 * <br />
 *
 * Before you start to write code, design your solution on paper using pseudocode. You will find it useful to refer to
 * the example on pages 20-21 of Java for Everyone. This example is similar to the problem you have been set, but not
 * identical.
 *
 * <br />
 * <br />
 *
 * Your task is the following:
 *
 * <br />
 * <br />
 *
 * Write a program to help a person calculate the total cost over 5 years of owning a car. Report the cost as a whole
 * number of pounds. The program takes the following inputs:
 *
 * <ul>
 * <li>The price of the new car;</li>
 * <li>The estimated resale value of the car after 5 years;</li>
 * <li>The estimated number of miles driven per year;</li>
 * <li>The estimated price of a litre of petrol;</li>
 * <li>The fuel efficiency of the car, in miles per litre;</li>
 * </ul>
 *
 * For simplicity, we shall ignore the cost of financing (assume that the buyer can afford to pay for the car
 * outright).
 *
 * <br />
 * <br />
 *
 * Test your program with realistic values for these inputs (search for these on the web or ask a car-owning friend for
 * advice). Fuel efficiency figures are often given in miles per gallon (MPG); you can convert these to miles per litre
 * on <a href="http://www.mpgtokpl.com/mpgtompl.htm">this website</a>.
 *
 * <br />
 * <br />
 *
 * Your program should include variables to hold all of the inputs listed above. Think carefully about the types of
 * these variables: for each one, decide whether it should be {@link java.lang.Integer int} or {@link java.lang.Double
 * double}.
 *
 * <br />
 * <br />
 *
 * You will find it useful to introduce additional variables to help with calculating the final result. For instance,
 * you could use a variable called {@code depreciation} to store the loss in value of the car over 5 years (this is the
 * price of the new car minus its resale value after 5 years). This will be just one component of the cost of the car.
 * Identify other components and introduce suitably named variables to store them.
 *
 * <br />
 * <br />
 *
 * Make sure your code is readable, well formatted, and includes suitable comments. Choose appropriate names for
 * variables and constants (avoid using “magic numbers”: see Programming Tip 2.2 on page 39 of Java for Everyone for an
 * explanation of what these are). Make your program user friendly by including informative prompts for input and
 * readable output.
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */
package data.dataB.dataB2;
