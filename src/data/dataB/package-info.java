/**
 * <p>
 * <i>Practical 4: Input, problem solving</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn how to write programs that read user input.</li>
 * <li>To practice solving a larger problem using the skills learnt so far.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 17.09.2018
 * @since 1.0
 */
package data.dataB;
