package decisions.decisionsA.decisionsA1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 5 <br />
 * Decisions A1 <br />
 * <code>SignChecker.java</code>
 *
 * <p>
 * A program to check the sign of an {@link java.lang.Integer integer}.
 * </p>
 *
 * @author Michael Sammels
 * @version 24.09.2018
 * @since 1.0
 */

public class SignChecker {
    /**
     * Constructor.
     */
    public SignChecker() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        System.out.print("Please enter a positive number, or a negative number. Any non-integer will quit: ");
        try (Scanner scan = new Scanner(System.in)) {
            if (scan.hasNextInt()) {
                int input = scan.nextInt();
                if (input > 0) {
                    System.out.println("\nThis number is positive");
                } else {
                    System.out.println("\nThis number is negative");
                }
            } else {
                System.err.println("\nNot an integer");
            }
        }
    }
}
