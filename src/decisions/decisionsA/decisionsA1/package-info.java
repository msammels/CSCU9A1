/**
 * <p>
 * Making Decisions: Part 1
 * </p>
 *
 * We will start with a very simple program which reads in an {@link java.lang.Integer integer} and reports if it is
 * positive.
 *
 * <ul>
 * <li>Create a BlueJ Project called <b>{@link SignChecker#SignChecker SignChecker}</b>, add to it a new class called
 * <b>{@link SignChecker#SignChecker SignChecker}</b>, and replace the code in this class with the code below (with
 * suitably modified comments). Remember that you can either type in the code or copy and paste it in from the web
 * version of this practical. If you use copy and paste, make sure to correct any formatting errors that are
 * introduced. Compile and run the program once you have typed it in.
 *
 * <pre>
 *         import java.util.Scanner;
 *
 *         {@literal /**}
 *          * A program to check the sign of an {@link java.lang.Integer integer}.
 *          * {@literal @author} Your Name
 *          * {@literal @version} Today's Date
 *          *&#47;
 *
 *         public class SignChecker {
 *             {@literal /**}
 *              * The main launcher method.
 *              * {@literal @param} args command line arguments (unused).
 *              *&#47;
 *             public static void main (String[] args) {
 *                 Scanner scan = new Scanner(System.in);
 *                 System.out.println ("Enter an integer");
 *                 int input = scan.nextInt();
 *
 *                 <mark>if (input > 0) {</mark>
 *                     <mark>System.out.println ("This number is positive.");</mark>
 *                 <mark>}</mark>
 *             }
 *         }
 *     </pre>
 *
 * Look at the highlighted section of code. This is an {@code if} statement: it begins with the keyword {@code if} and
 * is guarded by the {@link java.lang.Boolean Boolean} condition {@code (input > 0)}. The body of the {@code if}
 * statement is enclosed within braces. When the program is run, the {@link java.lang.Boolean Boolean} condition is
 * evaluated. If its value is {@code true}, then the statement(s) in the body of the {@code if} statement are executed.
 * If the value of the condition is {@code false}, then these statements are skipped.
 *
 * <br />
 * <br />
 *
 * The <b>{@link SignChecker#SignChecker SignChecker}</b> program does not do anything if the {@code if} statement
 * condition is {@code false}, so there is no output if the input is not greater than {@code 0}. We can change this by
 * adding an {@code else} part to the {@code if} statement.</li>
 * <li>Add the following code to the program immediately after the highlighted lines of code. Then compile and run your
 * program a few times, trying both positive and negative inputs.
 *
 * <pre>
 *         else {
 *             System.out.println ("This number is not positive.");
 *         }
 * </pre>
 *
 * Do you understand how the program works?</li>
 * <li>That was all pretty easy so far. Now let’s make things more interesting. If a number is not positive, there are
 * two possibilities: either it is equal to {@code 0} or it is negative. You can check if input is equal to {@code 0}
 * by using the condition {@code (input == 0)}. Replace the code inside the {@code else} block with another {@code if}
 * statement that distinguishes between these two cases and prints a suitable output in each case. Then test your
 * program by running it with a suitable set of inputs.
 *
 * <br />
 * <br />
 *
 * Did you get that working? Congratulations, you have just created a nested {@code if-else} statement!
 *
 * <br />
 * <br />
 *
 * We are going to make one final improvement to this program before moving on to the next project. Right now, the
 * program will crash if the user types in anything that is not an {@link java.lang.Integer integer}. (Try it – type in
 * a {@link java.lang.String string} or a decimal number and see what happens.) Good programs do not behave like this:
 * they are robust and can deal with anything the user throws at them.
 *
 * <br />
 * <br />
 *
 * To make this program robust we need to do a bit of <i>input validation</i> – this means checking that the input
 * provided is of the right type. The {@link java.util.Scanner Scanner} library that we use to read keyboard input
 * contains a method called{@link java.util.Scanner#hasNextInt hasNextInt()} that checks if the incoming input is an
 * {@link java.lang.Integer integer}. If this method returns {@code true}, that is, there is an
 * {@link java.lang.Integer integer} coming up in the input stream, then the program can go ahead and use
 * {@link java.util.Scanner#nextInt nextInt()} to read this {@link java.lang.Integer integer}. Otherwise, the program
 * should not attempt to read an {@link java.lang.Integer integer} but should do something sensible instead, like
 * displaying an error message.</li>
 * <li>Add the following {@code if-else} statement to your program immediately after the line that prompts the user to
 * enter an {@link java.lang.Integer integer}. Move all of your existing code for reading the {@link java.lang.Integer
 * integer}, testing its sign, and printing the output, into the {@code if} part of this statement. Indent the code you
 * moved into the {@code if} statement so that it is correctly formatted. Remember that you can indent several lines of
 * code at once by selecting them and pressing F6.
 *
 * <pre>
 * if (scan.hasNextInt()) {
 *     // Replace this comment with your existing code
 *     // for reading and processing the input
 * } else {
 *     System.out.println("Not an integer.");
 * }
 * </pre>
 *
 * </li>
 * <li>Compile and run your program and test it with a range of inputs, including things that are not
 * {@link java.lang.Integer integers}. Can it cope with anything you give it? Excellent!</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 24.09.2018
 * @since 1.0
 */
package decisions.decisionsA.decisionsA1;
