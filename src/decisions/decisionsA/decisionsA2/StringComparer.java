package decisions.decisionsA.decisionsA2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 5 <br />
 * Decisions A2 <br />
 * <code>StringComparer.java</code>
 *
 * <p>
 * This program implements an application which reads in two {@link java.lang.String strings} and compares them.
 * </p>
 *
 * @author Michael Sammels
 * @version 24.09.2018
 * @since 1.0
 */

public class StringComparer {
    /**
     * Constructor.
     */
    public StringComparer() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter two words separated by a space: ");
            String word1 = scan.next(), word2 = scan.next();

            /*
             * Validation
             *
             * Check if the strings are equal and which comes first in the dictionary Report both values to the standard
             * output
             */
            if (word1.equals(word2)) {
                System.out.println(word1 + " is equal to " + word2);
            } else {
                System.out.println(word1 + " is not equal to " + word2);
            }
            if (word1.compareTo(word2) < 0) {
                System.out.println(word1 + " comes before " + word2 + " in the dictionary");
            } else {
                // If both words are identical, report this to the standard output
                if (word1.compareTo(word2) == 0) {
                    System.out.println("These words are identical");
                } else {
                    System.out.println(word2 + " comes before " + word1 + " in the dictionary");
                }
            }
        }
    }
}
