/**
 * <p>
 * Making Decisions: Part 2
 * </p>
 *
 * In this part of the practical we shall look at how to write {@link java.lang.Boolean Boolean} conditions that
 * compare {@link java.lang.String strings}. We shall also practice using the {@link java.lang.Boolean Boolean}
 * operators to write {@code if-else} statements with complex conditions.
 *
 * <br />
 * <br />
 *
 * The first project is very similar to the previous project, but uses {@link java.lang.String strings} rather than
 * {@link java.lang.Integer integers}.
 *
 * <br />
 * <br />
 *
 * Create a program called <b>{@link StringComparer#StringComparer StringComparer}</b> which reads in two words. For
 * our purposes, consider a word to be any {@link java.lang.String string} that ends in a space, so it may contain
 * numbers and other symbols as well as letters. Store each word in a separate {@link java.lang.String String}
 * variable, call them {@code word1} and {@code word2}. The program must compare {@code word1} to {@code word2} and
 * report whether they are the same, or {@code word1} is alphabetically before {@code word2}, or {@code word2} is
 * alphabetically before {@code word1}. Below are some sample interactions with this program followed by some hints to
 * help you write it.
 *
 * <pre>
 * Program: Enter two words separated by a space
 * User: Stirling Castle
 *
 * Program: Enter two words separated by a space
 * User: Stirling Stirling
 * </pre>
 *
 * <p>
 * <b>Hints:</b>
 * </p>
 *
 * <ul>
 * <li>If your program contains a {@link java.util.Scanner Scanner} object named {@code scan}, you can use the method
 * {@link java.util.Scanner#next scan.next()} to read in a {@link java.lang.String string} terminated by a space. There
 * is no need for input validation because all sequences of characters are valid {@link java.lang.String strings}.</li>
 * <li>To compare the dictionary order of two {@link java.lang.String strings} {@code word1} and {@code word2}, use the
 * {@link java.lang.String#compareTo compareTo} method. This works as follows:
 *
 * <br />
 * <br />
 *
 * If {@code word1.compareTo(word2)} is negative, then {@code word1} comes before {@code word2} in the dictionary. If
 * it is positive then {@code word2} comes first, and if it is {@code 0} the two words are the same.
 *
 * <br />
 * <br />
 *
 * Alternatively, the condition {@link java.lang.String#equals word1.equals(word2)} can also be used to check if
 * {@code word1} and {@code word2} are the same.</li>
 * </ul>
 *
 * The next project is based on the childhood game “Rock, Paper, Scissors”. In case you are not familiar with this
 * game, it is a two-player game in which each person simultaneously chooses one of “rock”, “paper”, or “scissors”.
 * Rock beats scissors but loses to paper, paper beats rock but loses to scissors, and scissors beats paper but loses
 * to rock.
 *
 * <br />
 * <br />
 *
 * This project is an exercise in exploring different ways of writing complex combinations of {@code if- else}
 * statements. There is no single correct solution, but you will see that there are some solutions are better (more
 * readable, or more efficient) than others.
 *
 * <br />
 * <br />
 *
 * You are given the following code which prompts player 1 and player 2 to each enter a {@link java.lang.String
 * string}: rock, paper, or scissors, and displays that string. Create a project called
 * <b>{@link RockPaperScissors#RockPaperScissors RockPaperScissors}</b> containing this code. Compile and run the
 * program.
 *
 * <pre>
 * import java.util.Scanner;
 *
 * public class RockPaperScissors {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         Scanner scan = new Scanner(System.in);
 *         System.out.println("Player 1: Choose rock, paper, or scissors:");
 *         String player1 = scan.next().toLowerCase();
 *
 *         System.out.println("Player 2: Choose rock, paper, or scissors:");
 *         String player2 = scan.next().toLowerCase();
 *
 *         System.out.println("Player 1 chose " + player1);
 *         System.out.println("Player 2 chose " + player2);
 *
 *         // (Your code goes here...)
 *     }
 * }
 * </pre>
 *
 * The code above uses a {@link java.lang.String String} method you have not seen before,
 * {@link java.lang.String#toLowerCase toLowerCase()}. Can you guess what it does? If you are not sure, run your
 * program a few times giving it inputs like the following: rock, Rock, PaPeR, SCISSORS.
 *
 * <br />
 * <br />
 *
 * Complete the code by adding nested if statements to report “Player 1 wins”, “Player 2 wins”, or “It is a tie.”, as
 * appropriate.
 *
 * <br />
 * <br />
 *
 * Is your RockPaperScissors program heavily nested with {@code if} statements? It can be difficult to follow the logic
 * of any program that is heavily nested in this way. By constructing complex conditions with the {@code &&} and the
 * {@code ||} operators, it is possible to simplify the code and remove some of the {@code else} alternatives. Make a
 * copy of your project (call it <b>{@code RockPaperScissors2}</b>). In the new project, rewrite your program using
 * complex conditions. For example,
 *
 * <pre>
 * ((player1.equals("rock")) {@literal &&} (player2.equals("paper")))
 * </pre>
 *
 * describes the case where Player 1 has chosen Rock <i>and</i> Player 2 has chosen Paper.
 *
 * <br />
 * <br />
 *
 * [Some of you may have already used complex conditions when you wrote your code in the first place. That’s fine.
 * Before moving on to the next step, however, examine your code to see if there is any way to make it simpler and more
 * readable. For instance, how do you check for a tie? If you are explicitly checking all three combinations of inputs
 * that result in a tie, your code can probably be simplified.]
 *
 * <br />
 * <br />
 *
 * To complete your program, add code to make the program respond suitably if a user types in a {@link java.lang.String
 * string} that is not “rock”, “paper”, or “scissors”. You will probably need to use suitably-placed {@code if}
 * statements with complex conditions and may need to use the not operator {@code !}.
 *
 * <br />
 * <br />
 *
 * [I found this last step rather tedious and did not like how it spoiled the elegance of the code I created in the
 * previous step. Unfortunately, input validation is necessary for a program to be robust – we must not leave it out.
 * The good news is that later on we will learn how to use <i>methods</i> to separate off the input validation code so
 * that it does not make the rest of our code look messy.]
 *
 * @author Michael Sammels
 * @version 24.09.2018
 * @since 1.0
 */
package decisions.decisionsA.decisionsA2;
