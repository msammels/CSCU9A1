/**
 * <p>
 * <i>Practical 5: Making decisions</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To use {@code if} and {@code if-else} statements to make decisions.</li>
 * <li>To write {@link java.lang.Boolean Boolean} expressions to compare numbers and {@link java.lang.String Strings}.
 * </li>
 * <li>To use {@code if} statements to validate user input.</li>
 * <li>To make complex decisions with nested {@code if-else} statements.</li>
 * <li>To use {@link java.lang.Boolean Boolean} operators to express complex conditions.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 24.09.2018
 * @since 1.0
 */
package decisions.decisionsA;
