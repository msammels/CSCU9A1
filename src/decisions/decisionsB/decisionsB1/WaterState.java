package decisions.decisionsB.decisionsB1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 6 <br />
 * Decisions B1 <br />
 * <code>WaterState.java</code>
 *
 * <p>
 * This program implements an application which reads in temperature and altitude, and reports the state of the liquid
 * to the standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 01.10.2018
 * @since 1.0
 */

public class WaterState {
    /**
     * Constructor.
     */
    public WaterState() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        final int FREEZING_POINT = 0;
        int boiling_point = 100;
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("What is the temperature (" + "\u2103" + ")?: ");
            while (!scan.hasNextInt()) {
                System.err.println("Not an integer");
                scan.next();
            }
            int degrees = scan.nextInt();

            System.out.print("What is the altitude (metres above sea level)?: ");
            while (!scan.hasNextInt()) {
                System.err.println("Not an integer");
                scan.next();
            }
            int altitude = scan.nextInt();

            // Make sure that the altitude is not a negative number
            if (altitude < 0) {
                System.err.println("Altitude must be positive");
                scan.next();
            }
            boiling_point -= altitude / 300;
            if (degrees <= FREEZING_POINT) {
                System.out.println("Water is solid at these conditions");
            } else if (degrees < boiling_point) {
                System.out.println("Water is liquid at these conditions");
            } else {
                System.out.println("Water is gas at these conditions");
            }
        }
    }
}
