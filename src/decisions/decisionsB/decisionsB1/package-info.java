/**
 * <p>
 * Practice with Decisions: Part 1
 * </p>
 *
 * The first project in this practical will give you some practice writing {@code if-else} statements. This project is
 * based on programming exercises P3.9-11 from Chapter 3 of Java for Everyone.
 *
 * <br />
 * <br />
 *
 * Write a program (let’s call it <b>{@link WaterState#WaterState WaterState}</b>) which reads in temperature (in
 * &#x2103;) and altitude (in metres above sea level) and reports whether water is solid, liquid, or gaseous at the
 * given conditions. Assume that the boiling point of water goes down by 1 degree for each 300m rise in altitude, and
 * that the freezing point is unaffected by altitude. Also, assume that altitude cannot be negative.
 *
 * <br />
 * <br />
 *
 * The program must validate its input and produce user-friendly output. A user’s interaction with your program might
 * look like the following:
 *
 * <pre>
 * Program: What is the temperature (&#x2103;)?
 * User: 35
 *
 * Program: What is the altitude (metres above sea level)?
 * User: 5
 *
 * Program: Water is liquid at these conditions.
 * </pre>
 *
 * Hints / guidelines:
 *
 * <ul>
 * <li>To make your program easier to read and maintain, avoid using “magic numbers”. Instead, introduce appropriately
 * named constants to represent values such as the freezing point of water at sea level (0&#x2103;), the boiling point
 * of water at sea level (100&#x2103;), and the rise in altitude that causes the boiling point to drop by one &#x2103;
 * (300 metres).</li>
 * <li>If appropriate, simplify your code by introducing variables to store intermediate results during your
 * calculations.</li>
 * <li>Test your program with a suitable selection of input values.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 01.10.2018
 * @since 1.0
 */
package decisions.decisionsB.decisionsB1;
