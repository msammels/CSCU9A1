package decisions.decisionsB.decisionsB2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 6 <br />
 * Decisions B2 <br />
 * <code>Seasons.java</code>
 *
 * <p>
 * This program implements an application which reads in a day and month of a year and reports which season it is in to
 * the standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 01.10.2018
 * @since 1.0
 */

public class Seasons {
    /**
     * Constructor.
     */
    public Seasons() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter day and month (in dd MM format): ");
            int day = scan.nextInt(), month = scan.nextInt(), daysInMonth = 31;
            boolean validDate = true;
            if (month < 1 || month > 12 || day < 1 || day > daysInMonth) {
                validDate = false;
            } else if (month == 2) {
                daysInMonth = 29;
            } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                daysInMonth = 30;
            }
            if (validDate) {
                String season = "";
                switch (month) {
                case 1:
                case 2:
                case 3:
                    season = "Winter";
                    break;
                case 4:
                case 5:
                case 6:
                    season = "Spring";
                    break;
                case 7:
                case 8:
                case 9:
                    season = "Summer";
                    break;
                default:
                    season = "Autumn";
                    break;
                }
                if ((month % 3 == 0) && (day > 21)) {
                    season = switch (season) {
                    case "Winter" -> "Spring";
                    case "Summer" -> "Autumn";
                    default -> "Winter";
                    };
                }
                System.out.println("The season is " + season);
            } else {
                System.err.println("Invalid date entered. Try again");
            }
        }
    }
}
