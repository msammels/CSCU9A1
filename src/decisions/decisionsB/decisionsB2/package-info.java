/**
 * <p>
 * Practice with Decisions: Part 2
 * </p>
 *
 * The next project is based on problem P3.18 from Java for Everyone. Read ALL of the instructions carefully BEFORE you
 * start writing code.
 *
 * <br />
 * <br />
 *
 * The pseudocode below describes an algorithm which yields the season (Spring, Summer, Autumn, or Winter) for a given
 * month and day.
 *
 * <pre>
 * if month is 1, 2, or 3, set season to Winter
 * else if month is 4, 5, or 6, set season to Spring
 * else if month is 7, 8, or 9, set season to Summer
 * else set season to Autumn
 *
 * if month is divisible by 3 and day >= 21
 *     if season is Winter, set season to Spring
 *     else if season is Spring, set season to Summer
 *     else if season is Summer, set season to Autumn
 *     else set season to Winter
 * </pre>
 *
 * Write a program (call it <b>{@link Seasons#Seasons Seasons}</b>) which prompts the user for a month and a day and
 * outputs the corresponding season. An interaction with your program might look like the following:
 *
 * <pre>
 * Program: Enter day and month (dd mm):
 * User: 06 01
 *
 * Program: The season is Winter.
 * </pre>
 *
 * To find out if one number {@code m} is divisible by another number {@code n}, use the remainder operator {@code %}.
 * If {@code m} is divisible by {@code n} the remainder {@code m % n} will be equal to {@code 0}.
 *
 * <br />
 * <br />
 *
 * Your program must include robust input validation. As well as checking that the input is {@link java.lang.Integer
 * integers} (so the program does not crash), the program must ensure that the date is valid, <i>i.e.</i>:
 *
 * <ul>
 * <li>The month must be in the range 1 to 12</li>
 * <li>The day must be in the appropriate range for the month. If you are not sure what this range is, consult this
 * traditional rhyme:
 *
 * <pre>
 *         Thirty days hath September,
 *         April, June and November;
 *         All the rest have thirty-one,
 *         Excepting February alone,
 *         And that has twenty-eight days clear,
 *         And twenty-nine in each leap year.
 * </pre>
 *
 * </li>
 * </ul>
 *
 * Since your program does not ask for the year, assume that the allowed days in February are those in the range 1 to
 * 29.
 *
 * <br />
 * <br />
 *
 * If you try to combine the code for checking the validity of the date with the code for deciding which season it is,
 * your program will begin to look very complex and messy. A better approach is to check the validity of the date
 * first, storing the result in a {@link java.lang.Boolean boolean} variable (call it {@code validDate}). This variable
 * will then be used to decide whether to go ahead and determine the season or to display an error message.
 *
 * <br />
 * <br />
 *
 * Below is some pseudocode which you can use to check if the date is valid. As well as the {@link java.lang.Boolean
 * boolean} variable {@code validDate}, I also found it useful to use an {@link java.lang.Integer integer} variable
 * called {@code numDays} to store the number of days in the given month. This is initialised with the default value
 * 31, and this value is then changed if the month is one of those with fewer than 31 days.
 *
 * <pre>
 * set validDate to true
 * set numDays to 31
 * if month {@literal <} 1 or month > 12 or day {@literal <} 1, set validDate to false
 * else if month is 2, set numDays to 29
 *     else if month is 4, 6, 9 or 11, set numDays to 30
 *     if day > numDays set validDate to false
 * </pre>
 *
 * Once {@code validDate} has been given a value, this can be used to decide whether to find out and display the season
 * or to display an error message, as this pseudocode outline suggests:
 *
 * <pre>
 * if validDate
 *     [find out and display the season]
 * else
 *     [display a suitable error message]
 * </pre>
 *
 * This project involves some moderately complex logic. Test your program with an appropriate selection of inputs.
 *
 * @author Michael Sammels
 * @version 01.10.2018
 * @since 1.0
 */
package decisions.decisionsB.decisionsB2;
