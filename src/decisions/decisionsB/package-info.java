/**
 * <p>
 * <i>Practical 6: Practice with Decisions</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To practice using {@code if-else} statements and {@link java.lang.Boolean Boolean} conditions.</li>
 * <li>To use these new skills to implement more complex algorithms.</li>
 * <li>To practice testing programs by choosing suitable test cases.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 01.10.2018
 * @since 1.0
 */
package decisions.decisionsB;
