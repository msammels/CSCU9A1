package intro.introA.introA1;

/**
 * CSCU9A1 - Practical 1 <br />
 * Intro A1 <br />
 * <code>HelloWorld.java</code>
 *
 * <p>
 * This program implements an application that displays the traditional first-world message to the standard output.
 * </p>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */

public class HelloWorld {
    /**
     * Constructor.
     */
    public HelloWorld() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
