/**
 * <p>
 * Part 1: A First Taste of Java
 * </p>
 * Professional programmers usually use software tools called Integrated Development Environments (IDEs) to help them
 * create their programs. In CSCU9A1, we will be using an IDE called BlueJ which is specially designed for students.
 * However, it is instructive to see how to create a Java program without needing a sophisticated IDE. For your first
 * Java program, you will use just the bare essentials:
 *
 * <ul>
 * <li>a <i>text editor</i> to write the source code;</li>
 * <li>a <i>compiler</i> to translate the source code into executable bytecode;</li>
 * <li>a means to execute the compiled program.</li>
 * </ul>
 *
 * Before we can start, create a folder for storing your work:
 *
 * <br />
 * <br />
 *
 * Open your home folder (this is the <b>{@code H:}</b> drive on your computer). Inside your home folder, create a
 * folder called <b>{@code CSCU9A1}</b>. This is where you will keep your CSCU9A1 work.
 *
 * (<strong>Warning: do not store your work on the Desktop – it will vanish when you log out</strong>.)
 *
 * <br />
 * <br />
 *
 * A text editor is rather like a stripped-down word processor. It allows you to type in simple text files but does not
 * have fancy features like different fonts, colours, pictures and so on. The source code of a Java program is a simple
 * text file and can be typed in using a text editor.
 *
 * <br />
 * <br />
 *
 * Open the Notepad text editor (<b>Start &rarr; All Programs &rarr; Accessories &rarr; Notepad</b>). Type in the
 * following exactly as it appears below:
 *
 * <pre>
 * public class HelloWorld {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         System.out.println("Hello World!");
 *     }
 * }
 * </pre>
 *
 * When you have finished typing, go to the <b>File</b> menu and choose <b>Save As</b>. In the dialog box that appears,
 * navigate to your <b>{@code H:\CSCU9A1}</b> folder. Make the file name <b>{@link HelloWorld#HelloWorld
 * HelloWorld.java}</b>, change the file type to <b>All Files</b> and leave the encoding as <b>ANSI</b>. The image
 * below shows what the dialog box should look like. Click <b>Save</b> to save the file and then minimise or close
 * Notepad.
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img01.jpg" alt ="prac1-introA-img01" />
 * </p>
 *
 * Look at the contents of your <b>{@code H:\CSCU9A1}</b> folder. It should look like the image below. The folder
 * contains the file you just created, called <b>{@link HelloWorld#HelloWorld HelloWorld.java}</b>. This file contains
 * the source code of your Java program.
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img02.jpg" alt ="prac1-introA-img02" />
 * </p>
 *
 * The next step is to compile this source code into executable byte code. For this you must use the Java compiler,
 * <b><a href="https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javac.html">javac</a></b>.
 *
 * <br />
 * <br />
 *
 * To use the Java compiler outside of an IDE we need a <i>Command Prompt</i>. This is an application that allows you
 * to run basic programs (called <i>commands</i>) by typing their names in directly, rather than double-clicking on
 * icons or using the <b>Start</b> menu.
 *
 * <br />
 * <br />
 *
 * (The Command Prompt, also known as the “shell” or the “console”, is usually used by advanced computer users for
 * troubleshooting computer problems and performing administrative tasks. It has many useful commands and functions
 * which you can research for yourself if you are interested. In this practical we shall use only a few basic
 * commands.)
 *
 * <br />
 * <br />
 *
 * Open the Command Prompt (<b>Start &rarr; All Programs &rarr; Accessories &rarr; Command Prompt</b>). The Command
 * Prompt window appears.
 *
 * <br />
 * <br />
 *
 * Examine the Command Prompt window. It contains a “prompt” that looks like this: <b>{@code H:\>}</b>. The prompt
 * shows the name of a folder, in this case, your home folder, <b>{@code H:\}</b>. Commands are typed in next to the
 * prompt. If you type in a command that needs to use a file, the prompt will look for that file in the folder shown in
 * the prompt.
 *
 * <br />
 * <br />
 *
 * We want to use the Command Prompt to compile the <b>{@link HelloWorld#HelloWorld HelloWorld.java}</b> file, which is
 * in the folder <b>{@code H:\CSCU9A1}</b>. So the first step is to change to this folder, using the
 * <b>{@code chdir}</b> command. (<b>{@code chdir}</b> stands for “<b>ch</b>ange <b>dir</b>ectory”; “directory” is
 * another word for “folder”). Click next to the prompt, type in <b>{@code chdir CSCU9A1}</b>, then press return.
 *
 * <br />
 * <br />
 *
 * The Command Prompt window should change to look like the image below (not exactly: it will be a more recent Windows
 * version).
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img03.jpg" alt ="prac1-introA-img03" />
 * </p>
 *
 * The Command Prompt is now focused on your <b>{@code CSCU9A1}</b> folder, as you can see by the change in the prompt.
 * You can use the <b>{@code dir}</b> command to see the contents of this folder.
 *
 * <br />
 * <br />
 *
 * Try it now: type <b>{@code dir}</b> at the prompt and press return. The results may look rather cryptic but you
 * should be able to see that your file <b>{@link HelloWorld#HelloWorld HelloWorld.java}</b> is there.
 *
 * <br />
 * <br />
 *
 * Now that the Command Prompt is pointing at the correct folder, you can use the Java compiler
 * <b><a href="https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javac.html">javac</a></b> to compile your
 * program. Type in the following command at the prompt, and then press return.
 *
 * <pre>
 * javac HelloWorld.java
 * </pre>
 *
 * What happened? The result depends on how good you are at typing...
 *
 * <br />
 * <br />
 *
 * If you are an accurate typist and made no mistakes when you typed in the program (or you copy and pasted it), it may
 * seem as if nothing has happened. In fact, your program has been compiled successfully. You get to skip the next
 * step. Congratulations!
 *
 * <br />
 * <br />
 *
 * If, however, you are like most of us, you probably made one or two mistakes when you typed in the program. The Java
 * compiler will detect these syntax errors and complain about them. I left out the semicolon at the end of line 5, and
 * <b><a href="https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javac.html">javac</a></b> gave me this
 * error message:
 *
 * <pre>
 * HelloWorld.java:5: ';' expected
 *      System.out.println("Hello, World!")^
 * </pre>
 *
 * You may see a different error message, depending on what error(s) you made. If you have made errors, you need to fix
 * them before your program can be compiled.
 *
 * <br />
 * <br />
 *
 * (If you made syntax errors). Open your program in Notepad, fix the errors, and save the program. Then go back to the
 * Command Prompt and try to compile your program. Repeat these steps until your program compiles without any error
 * messages.
 *
 * <br />
 * <br />
 *
 * If you have reached this point, you have successfully compiled your program without making any syntax errors. Well
 * done!
 *
 * <br />
 * <br />
 *
 * The effect of compiling the program is to translate the source code (in the file <b>{@link HelloWorld#HelloWorld
 * HelloWorld.java}</b>) into an executable form called bytecode.
 *
 * <br />
 * <br />
 *
 * Look at the contents of your <b>{@code H:\CSCU9A1}</b> folder. (You can do this either by typing in the command
 * <b>{@code dir}</b> in the Command Prompt or by navigating to that folder in Windows Explorer.) Can you see the new
 * file that has been created? This file contains the executable bytecode form of your program.
 *
 * <br />
 * <br />
 *
 * The final step in the process is to run your program. Again we will use the Command Prompt to do this. The command
 * you need to use is called the <i>Java application launcher tool</i>, otherwise known as <b>{@code java}</b>.
 *
 * <br />
 * <br />
 *
 * In the Command Prompt window, type in the following:
 *
 * <pre>
 * java HelloWorld
 * </pre>
 *
 * You should now see the result of running your program. Congratulations! You have created your first Java program. To
 * finish, carry out the following task.
 *
 * <br />
 * <br />
 *
 * Modify the <b>{@link HelloWorld#HelloWorld HelloWorld}</b> program to make it print a few additional lines of text.
 * The content of this text is up to you. Compile your program, fix any errors and compile again if necessary.
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package intro.introA.introA1;
