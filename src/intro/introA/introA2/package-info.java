/**
 * <p>
 * Part 2: Introduction to the BlueJ IDE
 * </p>
 *
 * In Part 1 of this practical you saw how to create a Java program using the bare essentials: a text editor for typing
 * in the source code, the
 * <b><a href="https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javac.html">javac</a></b> command for
 * compiling the source code into bytecode, and the <b>{@code java}</b> command for executing the bytecode. The process
 * involved going back and forth between Notepad and the Windows Command Prompt, and you may have found this rather
 * tedious, especially if (like me) you tend to make lots of typing errors.
 *
 * <br />
 * <br />
 *
 * This is why professional programmers usually prefer to use a tool that integrates all of these different functions
 * into a single package: an Integrated Development Environment or IDE. IDEs provide all the functions mentioned above
 * and many others that you will learn about in due course. In this part of the practical you will be introduced to the
 * BlueJ IDE, which you will be using for the remainder of this module.
 *
 * <br />
 * <br />
 *
 * Start BlueJ (<b>Start &rarr; All Programs &rarr; BlueJ &rarr; BlueJ</b>). The BlueJ window appears. If this is the
 * first time you have used it, it should look like the screenshot below:
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img04.jpg" alt ="prac1-introA-img04" />
 * </p>
 *
 * When you use BlueJ, your work is organised into folders which BlueJ calls <i>projects</i>. Each project folder
 * contains all the files associated with a specific Java program. When BlueJ is used for the first time, it starts up
 * with no project opened, as shown in the screenshot above. In subsequent uses it will (helpfully) open the project
 * you were working on the last time that you used it.
 *
 * <br />
 * <br />
 *
 * You do not have any BlueJ projects yet. Let’s create one.
 *
 * <br />
 * <br />
 *
 * From BlueJ’s <b>Project</b> menu, select <b>New Project</b>. A dialog window appears asking you to choose the
 * location and name of your new project.
 *
 * <br />
 * <br />
 *
 * In the dialog window, navigate into your <b>{@code H:\CSCU9A1}</b> folder. In the file name input area, type in
 * <b>HelloWorld</b> (with no extension). Leave the “Files of type” field as <b>All Files</b>. The dialog window should
 * look like the screenshot below. Then click <b>Create</b>.
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img05.jpg" alt ="prac1-introA-img05" />
 * </p>
 *
 * The BlueJ window will change: it now displays the content of the project you just created (which is not very much
 * yet). It should look like this:
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img06.jpg" alt ="prac1-introA-img06" />
 * </p>
 *
 * Before you continue building this project, it is useful to go outside BlueJ and take a look at the folder and files
 * that have been created.
 *
 * <br />
 * <br />
 *
 * Use Windows Explorer to open your <b>{@code H:\CSCU9A1}</b> folder. Its contents should resemble the screenshot
 * below. You will see that BlueJ has created a <i>folder</i> called <b>{@code HelloWorld}</b>; this is the new BlueJ
 * project folder you just created. The files you created in Part 1 of the practical should also be there (unless you
 * moved or deleted them).
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img07.jpg" alt ="prac1-introA-img07" />
 * </p>
 *
 * Navigate into the <b>{@code HelloWorld}</b> folder to see what it contains. There should be two files. One is called
 * <b>{@code package.bluej}</b> and is basically a housekeeping file BlueJ creates to help keep track of the project.
 * The other is a text document called <b>{@code README.TXT}</b>. This is a file in which you can write your own
 * comments on the project. BlueJ will allow you to edit the file.
 *
 * <br />
 * <br />
 *
 * Now go back to the BlueJ window. You will see that it displays an icon like this
 * <img src="{@docRoot}/resources/prac1-introA-img08.jpg" alt ="prac1-introA-img08" height="42" width="35" />. This is
 * in fact BlueJ’s view of the file <b>{@code README.TXT}</b>. Double-click on this icon to open the file in BlueJ’s
 * text editor. Edit the file to suit yourself (for instance, you can make the project title Hello World and name
 * yourself as the author of the project). When you have finished, go to the <b>Class</b> menu and select <b>Save</b>
 * to save the file.
 *
 * <br />
 * <br />
 *
 * The project does not contain any program code yet. Let’s add some! Click on the button labelled <b>New Class</b>. In
 * the dialog that appears, type in <b>{@link HelloWorld#HelloWorld HelloWorld}</b> as the class name and select
 * <b>Class</b> as the class type. Then click <b>OK</b>.
 *
 * <br />
 * <br />
 *
 * The main BlueJ window now shows a second icon, labelled <b>{@link HelloWorld#HelloWorld HelloWorld}</b>. In fact,
 * BlueJ has created a new file called <b>{@link HelloWorld#HelloWorld HelloWorld.java}</b> and added it into the
 * <b>{@link HelloWorld#HelloWorld HelloWorld}</b> project folder. If you view the contents of this folder through
 * Windows Explorer, you will see that the new file has been added. (You may need to press F5 to refresh the contents
 * of the Explorer window.) The screenshots below show the BlueJ view and the Windows Explorer view of the new file.
 *
 * <br />
 * <br />
 *
 * <figure> <img src="{@docRoot}/resources/prac1-introA-img09.jpg" alt ="prac1-introA-img09" /> <figcaption> BlueJ
 * window showing the newly created <b>{@link HelloWorld#HelloWorld HelloWorld.java}</b> file </figcaption> </figure>
 *
 * <figure> <img src="{@docRoot}/resources/prac1-introA-img10.jpg" alt ="prac1-introA-img10" /> <figcaption>Windows
 * Explorer view of the same file</figcaption> </figure>
 *
 * <br />
 * <br />
 *
 * The stripy appearance of the file in BlueJ signifies that it has not been compiled yet. We shall add some code to
 * the file and then compile it.
 *
 * <br />
 * <br />
 *
 * Double-click on the <b>{@link HelloWorld#HelloWorld HelloWorld}</b> file icon in BlueJ to open the file in BlueJ’s
 * text editor.
 *
 * <br />
 * <br />
 *
 * You will see that BlueJ, in an attempt to be helpful, has already added some starter code to the file. Personally, I
 * prefer to delete this code and type in my own code from scratch.
 *
 * <br />
 * <br />
 *
 * Click anywhere in the code displayed in the editor window, type Ctrl-A to select all the code, and then press
 * Delete.
 *
 * <br />
 * <br />
 *
 * Now type in the code for the <b>{@link HelloWorld#HelloWorld HelloWorld}</b> program you created in Part 1. (As a
 * shortcut, you can open that code in Notepad and copy and paste it into the BlueJ editor.)
 *
 * <br />
 * <br />
 *
 * The editor window should look something like the screenshot below.
 *
 * <p>
 * <img src="{@docRoot}/resources/prac1-introA-img11.jpg" alt ="prac1-introA-img11" />
 * </p>
 *
 * Notice that BlueJ’s text editor offers many features not found in Notepad. The lines of the program are numbered.
 * Different colours are used to highlight certain features (e.g. shades of red for keywords such as
 * <span style="color:#8a0808;">public, static, class and void</span>, and green for string literals such as
 * <span style="color:#088a19;">“Hello, World!”</span>. Colouring is also used to show the nested structure of the
 * different sections of the code.
 *
 * <br />
 * <br />
 *
 * Most people (like me) find these features of BlueJ useful while a few others see them as a distraction. If you do
 * not like them you can turn them off. (I leave it to you to figure out how to do this.) But they are very useful as
 * they can draw your attention to mistyping etc., when text is not coloured as you would expect.
 *
 * <br />
 * <br />
 *
 * Now that we have typed in (or pasted in) the program code, we can ask BlueJ to compile it. There are a number of
 * obvious (and less obvious) ways to do this. They all work. Go ahead and compile the code.
 *
 * <br />
 * <br />
 *
 * If there are no syntax errors, you will notice that the stripes have disappeared from the
 * <b>{@link HelloWorld#HelloWorld HelloWorld}</b> icon in the main BlueJ window. BlueJ uses stripes to indicate
 * program files that have not yet been compiled.
 *
 * <br />
 * <br />
 *
 * f there are syntax errors, BlueJ will tell you about them and will highlight the place in the code where it thinks
 * the error is located. Correct the errors and compile the code again. Repeat this process until the code compiles
 * with no errors.
 *
 * <br />
 * <br />
 *
 * Remember what happened in Part 1: when you compiled the program, a new file containing executable bytecode was
 * created. Can you remember the name of this file? Compiling via BlueJ has the same effect – the executable bytecode
 * file is created. BlueJ does not show this file in its window, but if you look at the <b>{@code HelloWorld}</b>
 * project folder in Windows Explorer you will see that the file is there. (Don’t take my word for it – check for
 * yourself!)
 *
 * <br />
 * <br />
 *
 * The next step is to run the program. We can do this from within BlueJ. Here is how.
 *
 * <br />
 * <br />
 *
 * In the main BlueJ window, right click on the <b>{@link HelloWorld#HelloWorld HelloWorld}</b> icon. A menu will
 * appear. Choose the second item on the menu, which looks like this.
 *
 *
 *
 * <pre>
 * <b>void main(String [] args)</b>
 * </pre>
 *
 *
 *
 * This may look a little mysterious, but basically, you are asking BlueJ to run the main part (more correctly, the
 * <b>{@link HelloWorld#main main}</b> <i>method</i>) of the <b>{@link HelloWorld#HelloWorld HelloWorld}</b> program.
 *
 * <br />
 * <br />
 *
 * A dialog pops up for you to supply any further information the program might need. This program does not need any
 * more information, so click <b>OK</b>.
 *
 * <br />
 * <br />
 *
 * The results of your program should now appear for you to admire.
 *
 * <br />
 * <br />
 *
 * To complete the second task (and this practical) you need to explore BlueJ and find out some more about its
 * features. Find the answers to the following questions:
 *
 * <ol>
 * <li>How can you switch line numbering on/off in the editor?</li>
 * <li>What is the difference between syntax highlighting and scope highlighting? How can these be controlled?</li>
 * <li>Try running the <b>{@link HelloWorld#HelloWorld HelloWorld}</b> program several times. You will see that the
 * output from previous runs remains on the terminal screen. How can you clear the old output from the screen before a
 * new run?</li>
 * <li>Good programmers use indentation (extra spaces at the start of a line) to make their code more readable. When
 * indenting several lines of code, it is tedious to type in spaces in each line individually. How can you indent
 * several consecutive lines of code simultaneously with a single key-stroke?</li>
 * </ol>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package intro.introA.introA2;
