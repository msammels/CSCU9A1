/**
 * <p>
 * <i>Practical 1: Introduction to Java</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn how to create, compile and run simple Java programs using a text editor and the Windows console.</li>
 * <li>To learn how to use the BlueJ Integrated Development Environment.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package intro.introA;
