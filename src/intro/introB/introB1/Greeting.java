package intro.introB.introB1;

/**
 * CSCU9A1 - Practical 2 <br />
 * Intro B1 <br />
 * <code>Greeting.java</code>
 *
 * <p>
 * This program implements an application that displays the traditional first-world message to the standard output,
 * however it does so in another language.
 * </p>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */

public class Greeting {
    /**
     * Constructor.
     */
    public Greeting() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        System.out.println("Kon'nichiwa, Sekai!");
        System.out.println("こんにちは, 世界!");
    }
}
