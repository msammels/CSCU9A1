/**
 * <p>
 * Part 1: Java Programming Exercises
 * </p>
 *
 * You are asked to write Java programs to perform a number of tasks. For each task, you must create a separate BlueJ
 * project containing the Java program for that task. Refer to the last practical sheet if you need a reminder of the
 * steps involved in creating, compiling, and running a Java program in BlueJ.
 *
 * <br />
 * <br />
 *
 * There is somewhat less guidance provided in this practical compared to the last practical. We expect you to think
 * creatively, refer to the lecture notes and textbook, and work out your own solutions.
 *
 * <p>
 * <b>Task 1. {@link Greeting#Greeting Greeting}</b>
 *
 * Write a program that prints a friendly greeting of your choice in a language other than English.
 *
 * <br />
 * <br />
 *
 * [<b>Hint</b>: instead of creating a new BlueJ project from scratch, start with your
 * <b>{@link intro.introA.introA2.HelloWorld#HelloWorld HelloWorld}</b> project from the last practical. From BlueJ’s
 * <b>Project</b> menu, use <b>Save As</b> to save a copy of this project with a name of your choice. Then modify the
 * program code, remembering to change the name of the main class from <b>{@code HelloWorld}</b> to something more
 * appropriate. Notice that when you change the name of the class, BlueJ automatically changes the name of the file
 * containing the code to match the name of the class.]
 *
 * <p>
 * <b>Task 2. {@link SimpleArithmetic#SimpleArithmetic Simple Arithmetic}</b>
 * </p>
 *
 * Write a program that prints two lines on the screen as described below. For the first line, use the command
 * {@link java.lang.System#out System.out.print()}to print
 *
 * <pre>
 * The sum of the first ten positive integers is
 * </pre>
 *
 * on the screen without moving to a new line. Then print the sum of the first ten positive integers, 1 + 2 + ... + 10.
 * The program must calculate this sum and print the resulting value on the screen.
 *
 * <br />
 * <br />
 *
 * The second line should be similar to the first, but showing the text
 *
 * <pre>
 * The product of the first ten positive integers is
 * </pre>
 *
 * followed by the product of the first ten positive integers, 1 x 2 ... x 10. Note that Java, like many programming
 * languages uses an asterisk ({@code *}) for multiplication.
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package intro.introB.introB1;
