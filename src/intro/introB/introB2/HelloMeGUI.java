package intro.introB.introB2;

import javax.swing.JOptionPane;

/**
 * CSCU9A1 - Practical 2 <br />
 * Intro B2 <br />
 * <code>HelloMeGUI.java</code>
 *
 * <p>
 * This program implements an application that displays the traditional first-world message to a message dialog, using
 * my name.
 * </p>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */

public class HelloMeGUI {
    /**
     * Constructor.
     */
    public HelloMeGUI() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Hello, Michael!");
    }
}
