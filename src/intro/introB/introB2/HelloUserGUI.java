package intro.introB.introB2;

import javax.swing.JOptionPane;

/**
 * CSCU9A1 - Practical 2 <br />
 * Intro B2 <br />
 * <code>HelloUserGUI.java</code>
 *
 * <p>
 * This program implements an application that displays the user name as input by the end user themselves.
 * </p>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */

public class HelloUserGUI {
    /**
     * Constructor.
     */
    public HelloUserGUI() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        String name = JOptionPane.showInputDialog("What is your name?");
        JOptionPane.showMessageDialog(null, "Hello " + name + "!");
    }
}
