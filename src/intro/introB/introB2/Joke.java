package intro.introB.introB2;

import javax.swing.JOptionPane;

/**
 * CSCU9A1 - Practical 2 <br />
 * Intro B2 <br />
 * <code>Joke.java</code>
 *
 * <p>
 * This program implements an application that interacts with the user via a series of {@link javax.swing.JOptionPane
 * JOptionPane} objects in order to tell a joke.
 * </p>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */

public class Joke {
    /**
     * Constructor.
     */
    public Joke() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        String name = JOptionPane.showInputDialog("What is your name?");

        JOptionPane.showInputDialog("Hello " + name + ". Here is a joke. Knock, knock!");
        JOptionPane.showInputDialog("Beets!");
        JOptionPane.showMessageDialog(null, "Beets me!");
    }
}
