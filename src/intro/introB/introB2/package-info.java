/**
 * <p>
 * Part 2: Fun with GUIs
 * </p>
 *
 * Graphical User Interfaces, or GUIs, are part of most modern software. GUI features include windows, pop-up dialog
 * boxes, drop-down menus, clickable icons, mouse input and many other familiar concepts. In semester 2 you will learn
 * to write Java programs using a full range of GUI features. This semester we will focus on the core constructs of
 * Java, but in this part of the practical we take a sneaky look ahead at a simple GUI construct and use it to have
 * some fun.
 *
 * <br />
 * <br />
 *
 * Create a new BlueJ project called <b>{@link HelloWorldGUI#HelloWorldGUI HelloWorldGUI}</b>. Add a new class to the
 * project containing the following code:
 *
 * <pre>
 * import javax.swing.JOptionPane;
 *
 * public class HelloWorldGUI
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         JOptionPane.showMessageDialog(null, "Hello World!");
 *     }
 * }
 * </pre>
 *
 * Compile and run this program in the usual way.
 *
 * <br />
 * <br />
 *
 * Take a look at the first line in this program. It is an example of an {@code import} statement. One of the biggest
 * strengths of Java is that it is supplied with a vast array of <i>library packages</i> that provide many useful and
 * diverse functions. The {@code import} statement is used to import a library package into a program so that the
 * functions it provides can be used. Your program is importing part of the {@link javax.swing Swing} library package,
 * which provides a large range of GUI constructs.
 *
 * <br />
 * <br />
 *
 * The {@link HelloWorldGUI#main main} method in this program is very similar to the
 * {@link intro.introA.introA2.HelloWorld#main main} method in the <b>{@link intro.introA.introA2.HelloWorld#HelloWorld
 * HelloWorld}</b> program you wrote in the last practical. The difference is in where the output appears. The
 * <b>{@link intro.introA.introA2.HelloWorld#HelloWorld HelloWorld}</b> program used {@link java.lang.System#out
 * System.out.println()} to write “Hello World!” on the terminal window (usually called <i>standard output</i>). This
 * program, however, uses a method from the {@link javax.swing Swing} library to create a GUI object (a
 * {@link javax.swing.JOptionPane JOptionPane}) in which “Hello World!” is displayed. The
 * {@link javax.swing.JOptionPane JOptionPane} remains on the screen until the user clicks <b>OK</b>.
 *
 * <br />
 * <br />
 *
 * Let’s play around with some more programs using {@link javax.swing.JOptionPane JOptionPanes}.
 *
 * <br />
 * <br />
 *
 * Your next task is easy. Save a copy of your <b>{@link HelloWorldGUI#HelloWorldGUI HelloWorldGUI}</b> project with
 * the name <b>{@link HelloMeGUI#HelloMeGUI HelloMeGUI}</b>. Modify the code in the new project so that the program
 * shows “Hello ” followed by your name.
 *
 * <br />
 * <br />
 *
 * A {@link javax.swing.JOptionPane JOptionPane} can also be used to get information from a user. The program can store
 * this information in a <i>variable</i> and use it later on. (You will learn more about variables in lecture 2.)
 *
 * <br />
 * <br />
 *
 * Your next program will carry out some simple interaction with the user.
 *
 * <br />
 * <br />
 *
 * Create a new project called, say <b>{@link HelloUserGUI#HelloUserGUI HelloUserGUI}</b>. (As before, the easiest way
 * to do this is to use <b>Save As</b> to copy one of your previous projects giving it the new name.) Edit the code so
 * that it looks like this:
 *
 * <pre>
 * import javax.swing.JOptionPane;
 *
 * public class HelloUserGUI
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         String name = JOptionPane.showInputDialog("What is your name?");
 *         JOptionPane.showMessageDialog(null, "Hello " + name + "!");
 *     }
 * }
 * </pre>
 *
 * Compile and run this program.
 *
 * <br />
 * <br />
 *
 * When you have seen what the program does, take a closer look at the two lines of code inside its
 * {@link HelloUserGUI#main main} method.
 *
 * <br />
 * <br />
 *
 * The first line is:
 *
 * <pre>
 * String name = JOptionPane.showInputDialog("What is your name?");
 * </pre>
 *
 * This uses {@link javax.swing.JOptionPane#showInputDialog JOptionPane.showInputDialog("What is your name?")} to pop
 * up a {@link javax.swing.JOptionPane JOptionPane} containing the prompt “What is your name?” and an area for the user
 * to type in a reply. The user’s reply is then stored in a variable called {@code name}.
 *
 * <br />
 * <br />
 *
 * The second line is:
 *
 * <pre>
 * JOptionPane.showMessageDialog(null, "Hello " + name + "!");
 * </pre>
 *
 * This line creates a second {@link javax.swing.JOptionPane JOptionPane}, this time with no user input area. This
 * {@link javax.swing.JOptionPane JOptionPane} displays the text “Hello ”, then the contents of the variable name, then
 * an exclamation mark. Here, the symbol {@code +} is used to glue or concatenate two <i>{@link java.lang.String
 * strings}</i> (pieces of text).
 *
 * <br />
 * <br />
 *
 * (Recall that Java also uses {@code +} for adding numbers. Having different meanings for the same symbol is called
 * <i>overloading</i>, and is common in programming languages. The compiler is able to figure out which meaning is
 * intended by looking at the context in which the symbol appears. In this program, can you see how the compiler can
 * tell that the {@code +} means concatenation rather than addition?)
 *
 * <br />
 * <br />
 *
 * For your final task, I want you to think of a “Knock, knock” joke. If you don’t know one find one on the web. Create
 * a program which asks the user’s name, greets the user, and then delivers your joke. Use
 * {@link javax.swing.JOptionPane JOptionPanes} to manage the dialogue between the program and the user. Assume that
 * the user will cooperate and type in the correct responses, so there is no need to check what the user has typed. The
 * interaction might look like this:
 *
 * <pre>
 * Program: What is your name?
 * User: Michael
 *
 * Program: Hello, Michael! Here is a joke. Knock, knock!
 * User: Who’s there?
 *
 * Program: Cheese!
 * User: Cheese who?
 *
 * Program: Cheese a jolly good fellow!
 * </pre>
 *
 * (Please find a better joke to use!)
 *
 * <p>
 * <b>How to Share Your Programs</b>
 * </p>
 *
 * Are you proud of a program you have written? Would you like to share it with friends? BlueJ can be used to create a
 * standalone version of your program that you can distribute. The recipient will be able to run the program by simply
 * double-clicking on it. (The recipient’s computer must have Java installed, but that is pretty standard nowadays.)
 *
 * <br />
 * <br />
 *
 * To share your program it needs to be turned into an executable JAR file. A JAR file (Java ARchive file) is basically
 * a single file which aggregates and compresses all the files that make up a Java program. It is very similar to a ZIP
 * file.
 *
 * <br />
 * <br />
 *
 * A JAR file can be made executable by specifying which file contains the program’s main method.
 *
 * <br />
 * <br />
 *
 * To create an executable JAR file in Blue follow these steps:
 *
 * <ul>
 * <li>From BlueJ’s <b>Project</b> menu, choose <b>Create Jar File</b>.</li>
 * <li>In the dropdown menu labelled <b>Main class</b>, choose the file that contains the program’s main method.</li>
 * <li>If you want to share your source code, tick the checkbox labelled <b>Include source</b>. Leave this box unticked
 * if you want to share the runnable program but not its source code.</li>
 * <li>If you want to include the various housekeeping files that BlueJ creates for its own use, tick the checkbox
 * labelled <b>Include BlueJ project files</b>.</li>
 * <li>Click <b>Continue</b>. A dialog appears allowing you to choose a location and name for the JAR file (this does
 * not have to be the same as the name of the project). Choose these, then click <b>Create</b>.</li>
 * <li>That’s it! The program can now be run by simply double-clicking on the JAR file. The JAR file can be distributed
 * by email, copying, uploading to a website, or any other means.</li>
 * </ul>
 *
 * <i>Caveat</i>: if your program uses standard output (e.g., if it contains {@link java.lang.System#out
 * System.out.println()} statements) the JAR file will not run correctly when it is double clicked (because Java will
 * not know where standard output is). In this case the JAR file can be run within the Command Prompt (as you did in
 * part one of the last practical), using the command <b>{@code java –jar (filename.jar)}</b>. The Command Prompt
 * window will serve as standard output.
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package intro.introB.introB2;
