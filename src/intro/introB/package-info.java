/**
 * <p>
 * <i>Practical 2: Java Practice</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To practice writing small programs using numbers and {@link java.lang.String strings}.</li>
 * <li>To learn more about syntax errors.</li>
 * <li>To learn how to create a very simple Graphical User Interface.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package intro.introB;
