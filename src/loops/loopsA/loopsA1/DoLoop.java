package loops.loopsA.loopsA1;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A1 <br />
 * <code>DoLoop.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class DoLoop {
    /**
     * Constructor.
     */
    public DoLoop() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        int i = 0;
        do {
            System.out.println("i is " + i);
            i++;
        } while (i < 6);
    }
}
