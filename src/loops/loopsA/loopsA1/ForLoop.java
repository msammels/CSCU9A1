package loops.loopsA.loopsA1;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A1 <br />
 * <code>ForLoop.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class ForLoop {
    /**
     * Constructor.
     */
    public ForLoop() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        for (int i = 0; i < 6; i++) {
            System.out.println("i is " + i);
        }
    }
}
