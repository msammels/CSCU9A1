package loops.loopsA.loopsA1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A1 <br />
 * <code>InputLoop.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class InputLoop {
    /**
     * Constructor.
     */
    public InputLoop() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        System.out.print("Enter an integer. The program will continue to loop until an integer is entered: ");
        try (Scanner scan = new Scanner(System.in)) {
            while (!scan.hasNextInt()) {    // While non-integers are present...
                scan.next();                // ...read and discard input, then prompt again
                System.out.println("\nBad input. Enter an integer");
            }
            int input = scan.nextInt();

            System.out.println("\nYou entered " + input + "!");
        }
    }
}
