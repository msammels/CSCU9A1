package loops.loopsA.loopsA1;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A1 <br />
 * <code>InputSequence.java</code>
 *
 * <p>
 * This program takes input from the user until something is entered which is not an {@link java.lang.Integer integer}.
 * When this happens, the program will then calculate the total of the {@link java.lang.Integer integers} entered and
 * display this, along with how many {@link java.lang.Integer integers} were entered to the user.
 * </p>
 *
 * @author Michael Sammels
 * @version 08.10.208
 * @since 1.0
 */

public class InputSequence {
    /**
     * Constructor.
     */
    public InputSequence() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.println("Enter an integer to continue, or a non-integer value to finish. Then press return");

            // Counter to keep track of the number of integers entered and a variable to hold the actual values entered
            int count = 0, sum = 0;
            while (scan.hasNextInt()) {
                System.out.println("Enter an integer to continue, or a non-integer value to finish. Then press return");
                int in = scan.nextInt();
                sum = sum + in;
                count++;
            }
            System.out.println("\nYou entered " + count + " integers. The sum of your entries is " + sum);
        }
    }
}
