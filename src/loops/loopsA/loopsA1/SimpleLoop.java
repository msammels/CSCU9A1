package loops.loopsA.loopsA1;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A1 <br />
 * <code>SimpleLoop.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class SimpleLoop {
    /**
     * Constructor.
     */
    public SimpleLoop() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        int i = 0, limit = 6;
        while (i < limit) {
            System.out.println("i = " + i);
            i++;    // Increment the loop index
        }
    }
}
