/**
 * <p>
 * Simple Loops: Part 1
 * </p>
 *
 * Loops provide a mechanism for repeating a block of code called the <i>loop body</i>. We begin this lab by
 * experimenting with {@code while} loops, the simplest form of loop code. Many loops are controlled with a single
 * variable, which we refer to as the <i>loop control variable</i> or the <i>loop index</i>.
 *
 * <br />
 * <br />
 *
 * Consider the code below. What is the output the program produces? Work this out first on paper by hand tracing the
 * code. Then, check your result by adding the code to a new BlueJ project (call it <b>{@link SimpleLoop#SimpleLoop
 * SimpleLoop}</b>) and running it to see what it does. Were you right?
 *
 * <pre>
 * public class SimpleLoop {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         int i = 0;
 *         int limit = 6;
 *
 *         while (i {@literal <} limit) {
 *             System.out.println("i = " + i);
 *             i++;    // Increment the loop index
 *         }
 *     }
 * }
 * </pre>
 *
 * Consider the <b>{@link SimpleLoop#SimpleLoop SimpleLoop}</b> program again. Find the line that increments the loop
 * index. What do you think will happen if you comment this out? Try it and see.
 *
 * <br />
 * <br />
 *
 * [You can stop execution of a program in BlueJ by right-clicking on the icon in the main BlueJ window that looks like
 * this <img src="{@docRoot}/resources/prac7-loopsA-img01.jpg" alt ="prac7-loopsA-img01" height="11" width="50" /> and
 * choosing “Reset Java Virtual Machine”.]
 *
 * <br />
 * <br />
 *
 * Manipulating the loop control variable is a crucial skill in learning to write code with loops. Create different
 * modified versions of the <b>{@link SimpleLoop#SimpleLoop SimpleLoop}</b> program that output the following
 * sequences:
 *
 * <ul>
 * <li>1, 2, 3, 4, 5, 6</li>
 * <li>0, 2, 4, 6, 8, 10</li>
 * <li>10, 12, 14, 16, 18, ..., 98, 100</li>
 * </ul>
 *
 * Java provides three types of loops: {@code while}, {@code for}, and {@code do} (also called {@code do-while}).
 * Theoretically, they are interchangeable – any program you write with one kind of loop could be rewritten using any
 * of the other types of loops. As a practical matter, though, it is often the case that choosing the right kind of
 * loop will make your code easier to produce, debug, and read. It takes time and experience to learn to make the best
 * loop choice, so this is an exercise to give you some of that experience.
 *
 * <br />
 * <br />
 *
 * Rewrite the original <b>{@link SimpleLoop#SimpleLoop SimpleLoop}</b> program using a {@code for} loop. Repeat the
 * exercise again but this time use a {@code do-while} loop. Which form of loop seems to work best? Why?
 *
 * <br />
 * <br />
 *
 * Now that you have met the different loop constructs in Java, let’s put them to use. Earlier you saw how to use
 * {@code if} statements for input validation, displaying an error message if a user types in the wrong kind of input.
 * A more helpful approach is to give the user a second (and third...) chance to provide valid input. The code below
 * shows how a {@code while} loop can be used for this.
 *
 * <pre>
 * import java.util.Scanner;
 *
 * public class InputLoop {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main (String[] args) {
 *         Scanner scan = new Scanner(System.in);
 *         System.out.println ("Enter an integer");
 *
 *         while (!scan.hasNextInt()) { // While non-integers are present...
 *             scan.next();             // ...read and discard input, then prompt again
 *             System.out.println("Bad input. Enter an integer");
 *         }
 *         int input = scan.nextInt();
 *         System.out.println("You entered " + input + "!");
 *     }
 * }
 * </pre>
 *
 * Create a new BlueJ project called <b>{@link InputLoop#InputLoop InputLoop}</b> containing the code above. (Don’t
 * forget to add the usual explanatory comment at the start of the code.) Compile the code and run it a few times. Make
 * sure you understand how it works. You will be using this program as the basis of the next exercise.
 *
 * <br />
 * <br />
 *
 * Save the <b>{@link InputLoop#InputLoop InputLoop}</b> project as a new BlueJ project (call it
 * <b>{@link InputSequence#InputSequence InputSequence}</b>). Modify the program so that it repeatedly prompts the user
 * to enter a number. Keep a running total of the numbers the user enters and also keep a count of the number of
 * entries the user makes. The program should stop whenever the user enters something that is not an
 * {@link java.lang.Integer integer}. When the user has finished, print the number of entries the user has typed and
 * their sum.
 *
 * <br />
 * <br />
 *
 * A sample interaction with this program might look like this:
 *
 * <pre>
 * Program: Enter an integer to continue or a non-integer value to finish. Then press return.
 * User: 2
 *
 * Program: Enter an integer to continue or a non-integer value to finish. Then press return.
 * User: 6
 *
 * Program: Enter an integer to continue or a non-integer value to finish. Then press return.
 * User: 89
 *
 * Program: Enter an integer to continue or a non-integer value to finish. Then press return.
 * User: q
 *
 * Program: You entered 3 integers. The sum of your entries is 97.
 * </pre>
 *
 * [Hint: There is a slight catch to modifying the loop from the <b>{@link InputLoop#InputLoop InputLoop}</b> project
 * in order to solve this problem. Consider how the <b>{@link InputLoop#InputLoop InputLoop}</b> program worked: it
 * repeatedly read and discarded bad input until the user finally entered an {@link java.lang.Integer integer}. The
 * <b>{@link InputSequence#InputSequence InputSequence}</b> program, on the other hand, must repeatedly read and
 * process {@link java.lang.Integer integer} input until the user finally types in something that is not an
 * {@link java.lang.Integer integer}. Can you figure out how to change the loop code to achieve this?]
 *
 * <br />
 * <br />
 *
 * [Hint 2: Use suitable variables to keep track of the number of entries so far and the sum of the entries so far. You
 * will need to think about where to declare these variables, how to initialise them, and when and how to update them.]
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */
package loops.loopsA.loopsA1;
