package loops.loopsA.loopsA2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A2 <br />
 * <code>CountingStrings.java</code>
 *
 * <p>
 * This program will read in a sequence of {@link java.lang.String strings} input by the user and keep a count of how
 * many begin with the {@link java.lang.String string} "Stir". If the user types "q" the program will display a message
 * saying how many of the input {@link java.lang.String strings} started with "Stir".
 * </p>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class CountingStrings {
    /**
     * Constructor.
     */
    public CountingStrings() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            String valid = "Stir";
            boolean running = true;
            int count = 0;  // Counter to keep track of the number of strings entered
            do {
                System.out.print("Enter a string to continue or q to quit: ");
                String in = scan.nextLine();
                if (in.length() >= 4 && in.substring(0, 4).equals(valid)) {
                    count++;
                } else if (in.equalsIgnoreCase("q")) {
                    running = false;
                }
            } while (running);
            System.out.println("\n" + count);
        }
    }
}
