package loops.loopsA.loopsA2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A2 <br />
 * <code>EvenIntegers.java</code>
 *
 * <p>
 * This program will read in a sequence of {@link java.lang.Integer integers} input by the user and keep a count of how
 * many are even. Once the user enters something that is not an {@link java.lang.Integer integer}, the program will
 * display a message saying how many even {@link java.lang.Integer integers} were read in and then terminate.
 * </p>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class EvenIntegers {
    /**
     * Constructor.
     */
    public EvenIntegers() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter an integer to continue or a non-integer value to finish. Then press return: ");

            int count = 0;  // Counter to keep track of the number of integers entered
            while (scan.hasNextInt()) {
                System.out.print("Enter an integer to continue or a non-integer value to finish. Then press return: ");
                int in = scan.nextInt();
                if (in % 2 == 0) {
                    count++;
                }
            }
            System.out.println("\nYou entered " + count + " even integers");
        }
    }
}
