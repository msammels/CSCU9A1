package loops.loopsA.loopsA2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A2 <br />
 * <code>MinimumInput.java</code>
 *
 * <p>
 * This program reads in a sequence of {@link java.lang.Integer integers} and keeps track of the smallest
 * {@link java.lang.Integer integer} the user entered. If something other than an {@link java.lang.Integer integer} is
 * entered, the program displays the minimum {@link java.lang.Integer integer} entered and stops.
 * </p>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class MinimumInput {
    /**
     * Constructor.
     */
    public MinimumInput() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter an integer to continue or a non-integer value to finish. Then press return: ");
            boolean first = true;
            int min = 0, val;
            while (scan.hasNextInt()) {
                val = scan.nextInt();
                System.out.print("Enter an integer: ");
                if (first) {
                    min = val;
                    first = false;
                } else if (val < min) {
                    min = val;
                }
            }
            System.out.println("\nThe minimum value is " + min);
        }
    }
}
