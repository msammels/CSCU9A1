package loops.loopsA.loopsA2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A2 <br />
 * <code>PrintBinaryDigits.java</code>
 *
 * <p>
 * This program reads a positive {@link java.lang.Integer integer} and prints all of its binary digits.
 * </p>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class PrintBinaryDigits {
    /**
     * Constructor.
     */
    public PrintBinaryDigits() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter the decimal number: ");
            int in = scan.nextInt(), rem;
            StringBuilder answer = new StringBuilder();
            StringBuilder finalAnswer = new StringBuilder();
            do {
                rem = in % 2;
                in = in / 2;
                answer.append(rem);
            } while (in >= 1);
            for (int i = answer.length() - 1; i >= 0; i--) {
                finalAnswer.append(answer.charAt(i));
            }
            System.out.println(finalAnswer);
        }
    }
}
