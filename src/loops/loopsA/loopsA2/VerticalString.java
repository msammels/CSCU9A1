package loops.loopsA.loopsA2;

import java.util.Scanner;

/**
 * CSCU9A1 - Practical 7 <br />
 * Loops A2 <br />
 * <code>VerticalString.java</code>
 *
 * <p>
 * This program implements an application which reads in a {@link java.lang.String string} and prints each letter on
 * its own line.
 * </p>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class VerticalString {
    /**
     * Constructor.
     */
    public VerticalString() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            String message;
            int marker = 0, length;
            char current;

            System.out.print("Enter a string: ");
            message = scan.nextLine();
            length = message.length();
            for (int count = 0; count < length; count++) {
                current = message.charAt(marker);
                System.out.println(current);
                marker++;
            }
        }
    }
}
