/**
 * <p>
 * Simple Loops: Part 2
 * </p>
 *
 * In this part of the practical you will use your loop programming skills to carry out various tasks designed to give
 * you practice using common techniques for programming with loops. It will be useful to read Section 4.7 of Java for
 * Everyone before beginning these tasks.
 *
 * <br />
 * <br />
 *
 * Make sure your programs include appropriate input validation and user-friendly output. Your code must be well
 * formatted and clearly commented.
 *
 * <p>
 * <b>Task 1 [{@link EvenIntegers#EvenIntegers Counting even integers}]</b>
 * </p>
 *
 * Write a program which reads in a sequence of {@link java.lang.Integer integers} input by the user and keeps a count
 * of how many of them are even. If the user types in something that is not an {@link java.lang.Integer integer}, the
 * program should display a message saying how many even {@link java.lang.Integer integers} were read, and then stop.
 *
 * <br />
 * <br />
 *
 * [Hint: Start with a copy of the <b>{@link loops.loopsA.loopsA1.InputSequence#InputSequence InputSequence}</b>
 * project.]
 *
 * <br/>
 * <br />
 *
 * [Hint 2: You can use the {@link java.lang.Boolean Boolean} expression ({@code i % 2 == 0}) to test whether an
 * {@link java.lang.Integer integer} {@code i} is even. Even {@link java.lang.Integer integers} have a remainder of
 * {@code 0} when divided by {@code 2}, and will therefore make this expression {@code true}.]
 *
 * <p>
 * <b>Task 2 [{@link CountingStrings#CountingStrings Counting strings beginning with a given substring}]</b>
 * </p>
 *
 * This task is somewhat similar to Task 1 but will give you some more practice manipulating strings. Write a program
 * which reads a sequence of {@link java.lang.String strings} and keeps a count of how many of them begin with the
 * {@link java.lang.String#substring substring} “Stir”. If the user types in “q” the program should display a message
 * saying how many of the input strings started with “Stir”, and then stop.
 *
 * <br />
 * <br />
 *
 * [Hint: You can start from a copy of your solution to Task 1. However, you will need to make substantial changes in
 * order to read {@link java.lang.String strings} rather than {@link java.lang.Integer integers}, and to recognise when
 * the user has asked the program to stop. Consider introducing a {@link java.lang.Boolean Boolean} variable to control
 * the input loop. Initialise this variable to {@code true} and change it to {@code false} when the input “q” is read.]
 *
 * <br />
 * <br />
 *
 * [Hint 2: Use the {@link java.util.Scanner Scanner} method {@link java.util.Scanner#nextLine nextLine()} to read in a
 * {@link java.lang.String string} ending with a newline. You will need to use the {@link java.lang.String#substring
 * substring(start, pastEnd)} method to find the initial four characters in the input {@link java.lang.String string}.
 * Note that if you ask for a {@link java.lang.String#substring substring} that is longer than the
 * {@link java.lang.String string} that was read, the program will throw an exception and crash. To avoid this you must
 * check that the input {@link java.lang.String string} is long enough before asking for the initial
 * {@link java.lang.String#substring substring}. You can read more about {@link java.lang.String String} methods in
 * section 2.5 of Java for Everyone.]
 *
 * <p>
 * <b>Task 3 [{@link MinimumInput#MinimumInput Finding the minimum of a set of inputs}]</b>
 * </p>
 *
 * This task is Programming Exercise P4.6 from Java for Everyone.
 *
 * <br />
 * <br />
 *
 * Translate the pseudocode below into a Java program. The program reads a sequence of {@link java.lang.Integer
 * integers} and keeps track of the smallest {@link java.lang.Integer integer} read so far. If the user enters
 * something that is not an {@link java.lang.Integer integer}, the program displays the minimum
 * {@link java.lang.Integer integer} read and stops. Note the use of a {@link java.lang.Boolean Boolean} variable
 * “{@code first}” to distinguish when the {@link java.lang.Integer integer} being read is the first in the sequence,
 * so that this can be used to initialise the “{@code minimum}” variable.
 *
 * <pre>
 * Set a Boolean variable “first” to true.
 *
 * While another value has been read successfully
 *     If first is true
 *         Set the minimum to the value just read
 *         Set first to false
 *     Else if the value is less than the minimum
 *         Set the minimum to the value
 *
 * Print the minimum
 * </pre>
 *
 * [Hint: Start with a copy of the <b>{@link loops.loopsA.loopsA1.InputSequence#InputSequence InputSequence}</b>
 * project.]
 *
 * <p>
 * <b>Task 4 [{@link VerticalString#VerticalString Printing a string vertically}]</b>
 * </p>
 *
 * This task is Programming Exercise P4.6 from Java for Everyone.
 *
 * <br />
 * <br />
 *
 * Write a program that reads in a word (a {@link java.lang.String string} terminated by a space) and prints each
 * character of the word on a separate line. For example, if the user provides the input “Harry”, the program prints
 *
 * <pre>
 * H
 * a
 * r
 * r
 * y
 * </pre>
 *
 * [Hint: you will need to use the {@link java.util.Scanner Scanner} method {@link java.util.Scanner#next next()} and
 * the {@link java.lang.String String} methods {@link java.lang.String#length length()} and
 * {@link java.lang.String#charAt charAt()}. See section 2.5 of Java for Students to learn more about these methods.]
 *
 * <br />
 * <br />
 *
 * [Hint: think carefully about the most appropriate kind of loop to use. Should you pick {@code while}, {@code for},
 * or {@code do-while}?]
 *
 * <p>
 * <b>Task 5 [{@link PrintBinaryDigits#PrintBinaryDigits Printing the binary digits of a number}]</b>
 * </p>
 *
 * This task is Programming Exercise P4.14 from Java for Everyone.
 *
 * <br />
 * <br />
 *
 * Write a program that reads a positive {@link java.lang.Integer integer} and prints all of its binary digits. The
 * pseudocode below describes how to do this:
 *
 * <pre>
 * Read i
 *
 * While i > 0
 *     Print the remainder i % 2
 *     Set i to i / 2
 * </pre>
 *
 * Note that the binary number will be printed backwards, that is, with the least significant digit first. So for
 * instance, if the user provides the input {@code 13}, the output will be {@code 1011}, whereas the binary
 * representation of {@code 13} is {@code 1101}. We shall see how to solve this problem in practical 10.
 *
 * <br />
 * <br />
 *
 * [Hint: Since you are reading in just a single {@link java.lang.Integer integer}, start with a copy of the
 * <b>{@link loops.loopsA.loopsA1.InputLoop#InputLoop InputLoop}</b> project from the start of the practical. Call your
 * new project <b>{@link PrintBinaryDigits#PrintBinaryDigits PrintBinaryDigits}</b>.]
 *
 * <br />
 * <br />
 *
 * [Hint 2: Just for now, assume that the user will not type in a negative number. We will improve the input validation
 * in this program when we revisit it in practical 10.]
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */
package loops.loopsA.loopsA2;
