/**
 * <p>
 * <i>Practical 7: Simple Loops</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn how to use {@code while}, {@code for} and {@code do-while} loops.</li>
 * <li>To become familiar with common ways of programming with loops.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */
package loops.loopsA;
