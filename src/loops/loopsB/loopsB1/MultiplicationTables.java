package loops.loopsB.loopsB1;

/**
 * CSCU9A1 - Practical 8 <br />
 * Loops B1 <br />
 * <code>MultiplicationTables.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class MultiplicationTables {
    /**
     * Constructor.
     */
    public MultiplicationTables() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 10; j++) {
                int product = i * j;
                System.out.println(i + "*" + j + " = " + product);
            }
            System.out.print("\n");
        }
    }
}
