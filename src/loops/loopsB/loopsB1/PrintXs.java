package loops.loopsB.loopsB1;

/**
 * CSCU9A1 - Practical 8 <br />
 * Loops B1 <br />
 * <code>PrintXs.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class PrintXs {
    /**
     * Constructor.
     */
    public PrintXs() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < i + 1; j++) {
                System.out.print("X ");
            }
            System.out.println();
        }
    }
}
