package loops.loopsB.loopsB1;

/**
 * CSCU9A1 - Practical 8 <br />
 * Loops B1 <br />
 * <code>SimpleNestedForLoop.java</code>
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */

public class SimpleNestedForLoop {
    /**
     * Constructor.
     */
    public SimpleNestedForLoop() {
    }

    /**
     * The main launcher method.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 3; j++) {
                System.out.println(i + " " + j);
            }
        }
    }
}
