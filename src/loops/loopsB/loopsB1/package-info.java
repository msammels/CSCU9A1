/**
 * <p>
 * Nested Loops: Part 1
 * </p>
 *
 * In this part of the practical we look at nested loops (loops within loops) and how they can be used.
 *
 * <br />
 * <br />
 *
 * Consider the code below. Hand-trace the code to work out what it does. Check your result by adding the code to a new
 * BlueJ project (call it <b>{@link SimpleNestedForLoop#SimpleNestedForLoop SimpleNestedForLoop}</b>) and running it to
 * see what it does. Did you get it right?
 *
 * <pre>
 * public class SimpleNestedForLoop {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         for (int i = 1; i {@literal <=} 5 ; i++) {
 *             for (int j = 1; j {@literal <=} 3; j++) {
 *                 System.out.println(i + " " + j);
 *             }
 *         }
 *     }
 * }
 * </pre>
 *
 * Experiment by giving various different values for the bounds of the loop control variables {@code i} and {@code j}.
 * Make sure you understand how your changes affect the program’s behaviour.
 *
 * <br />
 * <br />
 *
 * Make a copy of the previous project (call it <b>{@link MultiplicationTables#MultiplicationTables
 * MultiplicationTables}</b>). Change the code to create a program which outputs multiplication tables up to 10 times
 * 9. Format your output nicely. It should look like the following.
 *
 * <pre>
 * 1 * 2 = 2
 * 2 * 2 = 4
 * 3 * 2 = 6
 * 4 * 2 = 8
 * 5 * 2 = 10
 * 6 * 2 = 12
 * 7 * 2 = 14
 * 8 * 2 = 16
 * 9 * 2 = 18
 * 10 * 2 = 20
 * .
 * .
 * .
 * 1 * 9 = 9
 * 2 * 9 = 18
 * 3 * 9 = 27
 * 4 * 9 = 36
 * 5 * 9 = 45
 * 6 * 9 = 54
 * 7 * 9 = 63
 * 8 * 9 = 72
 * 9 * 9 = 81
 * 10 * 9 = 90
 * </pre>
 *
 * [Hint: To see all of your output, you may need to turn on “Unlimited buffering” in the Options menu in BlueJ’s
 * terminal window.]
 *
 * <br />
 * <br />
 *
 * Create a new project (call it <b>{@link PrintXs#PrintXs PrintXs}</b>) which uses nested for loops to produce the
 * following output:
 *
 * <pre>
 *  X
 *  XX
 *  XXX
 *  XXXX
 *  XXXXX
 * </pre>
 *
 * The outer loop can control the number of rows that will be printed. The inner loop can control the number of Xs that
 * print on a single line. The trick is to notice that there is a relationship between the row number and the number of
 * Xs in the row. This relationship allows you to use the outer loop control variable to control the inner loop.
 *
 * <br />
 * <br />
 *
 * The next project is based on Programming Exercise P4.21 from Java for Everyone.
 *
 * <br />
 * <br />
 *
 * Create a new project (call it <b>{@link DrawTriangle#DrawTriangle DrawTriangle}</b>). Write a program that reads an
 * {@link java.lang.Integer integer} and displays, using Xs, a triangle of the given height. For example, if the height
 * is 4, the program displays:
 *
 * <pre>
 *     X
 *    XXX
 *   XXXXX
 *  XXXXXXX
 * </pre>
 *
 * [Hint: This is a slightly more challenging task than the previous ones and will give you some practice in
 * calculating loop bounds and using loop control variables. My solution used an outer {@code for} loop to draw the
 * rows of the triangle, with <i>two</i> {@code for} loops inside it, the first to draw the spaces at the start of each
 * row, and the second to draw the Xs.]
 *
 * <br />
 * <br />
 *
 * [Hint 2: For reading in the height, you will find it useful to reuse code from the
 * <b>{@link loops.loopsA.loopsA1.InputLoop InputLoop}</b> project from practical 7.]
 *
 * <br />
 * <br />
 *
 * Once you’ve got that working, modify <b>{@link DrawTriangle#DrawTriangle DrawTriangle}</b> so that instead of asking
 * the user to enter height of the triangle, it simply asks if the user would like to see a triangle. If the user
 * answers “y”, the program will display a triangle with a randomly chosen height between 1 and 20. If the user gives
 * any other response, the program stops.
 *
 * <br />
 * <br />
 *
 * [Hint: to assign an {@link java.lang.Integer integer} variable called {@code height} a random number between bounds
 * {@code a} and{@code b} (inclusive), use this code:
 *
 * <pre>
 * height = (int) (Math.random() * (b – a + 1) + a);
 * </pre>
 *
 * Read more about random numbers in Section 4.9.1 of Java for Everyone.]
 *
 * <br />
 * <br />
 *
 * For the final step, modify <b>{@link DrawTriangle#DrawTriangle DrawTriangle}</b> so that after the triangle is
 * drawn, the program asks if the user would like another one. The program will repeatedly draw triangles of random
 * height until the user finally answers this question with something other than “y”.
 *
 * <br />
 * <br />
 *
 * [Hint: use a {@link java.lang.Boolean Boolean} variable to indicate whether the user wants to see more triangles or
 * not. Use this variable to control a {@code while} loop enclosing your existing code for drawing a triangle. You will
 * need to think about how to initialise this variable and when to change its value.]
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */
package loops.loopsB.loopsB1;
