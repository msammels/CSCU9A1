/**
 * <p>
 * Nested Loops: Part 2
 * </p>
 *
 * In this part of the practical you will get a chance to exercise all of the skills you have learnt so far to solve a
 * larger problem. You will write a program to allow two users to play the ancient game of Nim. According to Wikipedia
 * this game may have originated in China and is similar to a Chinese game called “picking stones”. Nowadays, it exists
 * in several variations. The version you will implement is described by the rules below.
 *
 * <br />
 * <br />
 *
 * <i> The game begins with a heap containing from 10 to 20 stones. Two players take turns to remove from 1 to 4 stones
 * from the heap. The player who removes the last stone wins. </i>
 *
 * <br />
 * <br />
 *
 * The pseudocode below describes the overall design of the program you must write:
 *
 * <pre>
 * Show instructions
 * Set heapSize to a random integer between 10 and 20
 * Set player to “Player 1”
 *
 * While heapSize > 0
 *     Display heapSize
 *     Ask player to choose how many stones to remove
 *     Reduce heapSize by the number of stones removed
 *
 *     If heapSize > 0
 *         Set player to the other player
 *
 * Display winner
 * </pre>
 *
 * The pseudocode above is rather abstract and leaves out a number of details which you will need to fill in when you
 * write the code. The main simplification is the step “Ask player to choose how many stones to remove”. For the
 * program to be robust and perform correctly, the user’s input must be validated. As well as checking that the input
 * is an {@link java.lang.Integer integer}, the program must also check that the input is in the permitted range (1 to
 * 4 stones), and that the player is not trying to remove more stones than are left in the pile. The pseudocode below
 * describes this step in more detail:
 *
 * <pre>
 * Prompt user to enter number of stones to remove
 * Set a Boolean variable moveRead to false
 *
 * While moveRead is false
 *     Read in an integer and assign it to stonesRemoved
 *     If stonesRemoved is in the allowed range and is less than heapSize
 *         Set moveRead to true
 *     Else
 *         Display error message
 *         Prompt user to enter number of stones to remove
 * </pre>
 *
 * In this pseudocode, the line “Read in an integer and assign it to stonesRemoved” also hides some implementation
 * detail. In practice, as you know by now, to prevent crashing the program should first check that there is an
 * {@link java.lang.Integer integer} in the input stream before attempting to read one in. You can reuse code from the
 * <b>{@link loops.loopsA.loopsA1.InputLoop InputLoop}</b> project in the previous practical to implement this line.
 *
 * <br />
 * <br />
 *
 * Here is an example of a game of {@link Nim#Nim Nim} using my version of this program. Your version does not have to
 * look identical to this but should work in the same way and handle the kinds of errors shown here.
 *
 * <pre>
 * ************************************************************************
 * *                      Welcome to the game of Nim!                     *
 * *                                                                      *
 * * Instructions: This is a game for two players. There is a heap        *
 * * containing 10 to 20 stones. Players take turns to remove from 1 to 4 *
 * * stones from the heap. The player who removes the last stone wins.    *
 * *                                                                      *
 * ************************************************************************
 *
 * Number of stones in the heap: 14
 * Player 1: how many stones will you remove (1 to 4)?
 * 3
 *
 * Number of stones in the heap: 11
 * Player 2: how many stones will you remove (1 to 4)?
 * x
 *
 * Bad input, try again.
 * Player 2: how many stones will you remove (1 to 4)?
 * 4
 *
 * Number of stones in the heap: 7
 * Player 1: how many stones will you remove (1 to 4)?
 * 5
 *
 * That is not possible, try again.
 * Player 1: how many stones will you remove (1 to 4)?
 * 4
 *
 * Number of stones in the heap: 3
 * Player 2: how many stones will you remove (1 to 4)?
 * 4
 *
 * That is not possible, try again.
 * Player 2: how many stones will you remove (1 to 4)?
 * 3
 *
 * Player 2 wins!
 * </pre>
 *
 * <p>
 * <b>Optional</b>
 * </p>
 *
 * If you have completed the practical and want to try something a little more challenging, modify your program t allow
 * a person to play against the computer. You will need to write code for the computer to choose its move. How will it
 * do this? Will it make a random choice or apply some clever strategy?
 *
 * <br />
 * <br />
 *
 * In fact, Nim has been studied extensively by game theorists, the theory behind it is well-understood, and winning
 * strategies have been identified. There is an accessible discussion of Nim strategy
 * <a href="http://www.eserc.stonybrook.edu/wise/HSfall2000/nim.html">here</a>.
 *
 * @author Michael Sammels
 * @version 08.10.2018
 * @since 1.0
 */
package loops.loopsB.loopsB2;
