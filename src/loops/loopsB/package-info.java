/**
 * <p>
 * <i>Practical 8: Nested loops</i>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn how to use nested loops.</li>
 * <li>To learn how to use random numbers.</li>
 * <li>To practice these new skills within a larger problem.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 10.09.2018
 * @since 1.0
 */
package loops.loopsB;
