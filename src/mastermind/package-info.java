/**
 * <p>
 * <b>Practical 11: Programming Challenge</b>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To use all the programming skills you have learned to create a program for playing the game of
 * {@link Mastermind#Mastermind Mastermind} against the computer.</li>
 * </ul>
 *
 * <p>
 * <b>Further Documentation</b>
 * </p>
 *
 * The <a href="http://bit.ly/1VnFT8U">Wikipedia</a> page on the game of Mastermind has a very good description of the
 * game and some links for further reading.
 *
 * <p>
 * <b>The Game of Mastermind</b>
 * </p>
 *
 * Mastermind is a code-breaking game of logic for two players that has been around since the 1970s. You may have
 * played it yourself.
 *
 * <br />
 * <br />
 *
 * In the game, Player One chooses a sequence of four coloured pegs, and hides this from Player Two. For example,
 * Player One might choose this sequence:
 *
 * <br />
 * <br />
 *
 * [Secret code is <span style="color:red;">Red</span>, <span style="color:blue;">Blue</span>,
 * <span style="color:#999900;">Yellow</span>, <span style="color:blue;">Blue</span>]
 *
 * <br />
 * <br />
 *
 * Player Two then tries to guess the hidden code. Player Two might guess the sequence below:
 *
 * <br />
 * <br />
 *
 * [Guess is <span style="color:#999900;">Yellow</span>, <span style="color:#999900;">Yellow</span>,
 * <span style="color:008000;">Green</span>, <span style="color:blue;">Blue</span>]
 *
 * <br />
 * <br />
 *
 * Player One then uses small black and white pegs to give feedback to player two. A black peg indicates a peg in the
 * guess that is a full match with a peg in the secret code, that is, it has the same colour and is in the same
 * position. A white peg indicates a peg in the guess that is a partial match with a peg in the secret code, that is,
 * it has the same colour but is in a different position. In the example, there is one full match (the blue peg) and
 * one partial match (a yellow peg). The second yellow peg in the guess is not considered a partial match because there
 * is only one yellow peg in the secret code.
 *
 * <br />
 * <br />
 *
 * Player Two then tries to make a better guess, and again receives feedback from Player One. The game continues until
 * either Player Two wins by guessing the correct sequence, or Player One wins because Player Two has run out of
 * chances to guess.
 *
 * <br />
 * <br />
 *
 * In the original game the code pegs were of six different colours and the maximum number of guesses was 12. The game
 * can be made harder by using more colours or allowing fewer guesses.
 *
 * <br />
 * <br />
 *
 * Your task is to write a program which lets the computer take the role of Player One, choosing the secret code and
 * giving feedback. The user will be Player Two and will try to guess the code.
 *
 * <br />
 * <br />
 *
 * A session with my program looks like this:
 *
 * <pre>
 * Welcome to the game of Mastermind!
 * I will choose a secret code of four colours.
 * The colours can be (R)ed, (O)range, (Y)ellow, (G)reen, (B)lue, or (P)urple
 * You have 12 attempts to try to guess my code.
 * Enter your guess like this example: BYYG
 * After each guess, I will you how well you have done.
 * o means that you have guessed a correct colour in the wrong position.
 * * means that you have guessed a correct colour in the right position.
 * Would you like to play a game (y/n)?
 *
 * <span style="color:blue">User: y</span>
 * <span style="color:red">[Please show the secret code to allow testing: secret code is RRBR]</span>
 * You have 12 guesses left.
 * Please enter your guess. Valid colours are ROYGBP.
 *
 * <span style="color:blue">User: ROGG</span>
 * Your feedback: *
 * You have 11 guesses left.
 * Please enter your guess. Valid colours are ROYGBP.
 *
 * <span style="color:blue">User: YPBG</span>
 * Your feedback: *
 * You have 10 guesses left.
 * Please enter your gues. Valid colours are ROYGBP.
 *
 * <span style="color:blue">User: BBRR</span>
 * You feedback: *oo
 * You have 9 guesses left.
 * Please enter your guess. Valid colours are ROYGBP.
 *
 * <span style="color:blue">User: RRBR</span>
 * Your feedback: ****
 * Congratulations, you won in 4 guesses!
 * </pre>
 *
 * @author Michael Sammels
 * @version 22.10.2018
 * @since 1.0
 */
package mastermind;
