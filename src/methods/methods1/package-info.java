/**
 * <p>
 * Methods: Part 1
 * </p>
 *
 * Consider the program below. This program contains a method called {@link Rectangle#area area} which is called three
 * times from the {@link Rectangle#main main} method.
 *
 * <pre>
 * public class Rectangle {
 *     {@literal /**}
 *      * The main launcher method.
 *      * {@literal @param} args command line arguments (unused).
 *      *&#47;
 *     public static void main(String[] args) {
 *         System.out.println (area(5, 3));
 *         System.out.println (area(6, 2));
 *         System.out.println (area(8, 3));
 *     }
 *
 *     {@literal /**}
 *      * Returns the area of a rectangle.
 *      * {@literal @param} length the length of the rectangle.
 *      * {@literal @param} width the width of the rectangle.
 *      * {@literal @return} The area ({@code
 * length
 * } * {@code
 * width
 * }).
 *      *&#47;
 *     public static int area(int length, int width) {
 *         return length * width;
 *     }
 * }
 * </pre>
 *
 * Study the code and think about the questions below.
 *
 * <ul>
 * <li>What is the result of running this program?</li>
 * <li>What is the <i>return type</i> of the {@link Rectangle#area area} method?</li>
 * <li>What are the names and types of the <i>parameter variables</i> of the {@link Rectangle#area area} method?</li>
 * <li>What are the values of the <i>arguments</i> provided when the {@link Rectangle#area area} method is called?</li>
 * </ul>
 *
 * Create a project called <b>{@link Rectangle#Rectangle Rectangle}</b> with a class containing the code above. Then
 * make the following changes:
 *
 * <ul>
 * <li>Add a call to the <b>{@link Rectangle#main main}</b> method to print the area of a rectangle with length 10 and
 * width 15</li>
 * <li>Add a method called {@link Rectangle#perimeter perimeter} which, when passed the length and width of a
 * rectangle, returns the value of its perimeter (the distance around it). Add some calls to this method from the
 * {@link Rectangle#main main} program.</li>
 * </ul>
 *
 * Good programmers use comments to explain the purpose of a method and the meaning of its parameters. It is a standard
 * practice for comments to include <i>Javadoc</i> tags which can be processed by automatic documentation generator
 * programs.
 *
 * <br />
 * <br />
 *
 * Add the comment below just before the {@link Rectangle#area area} method. Then add a similar comment to your
 * {@link Rectangle#perimeter perimeter} method.
 *
 * <pre>
 * {@literal /**}
 *  * Returns the area of a rectangle.
 *  * {@literal @param} height the height of the rectangle.
 *  * {@literal @param} width the width of the rectangle.
 *  *&#47;
 * </pre>
 *
 * You should be able to see the documentation generated from your comments by going to the dropdown menu at the top
 * right of the BlueJ editor window and selecting Documentation. Switch back to Source Code view when you are finished.
 *
 * <br />
 * <br />
 *
 * For the next task, we will make the program produce more user-friendly output while using methods to improve the
 * structure of the program. Write a method called {@link Rectangle#printRectangleDetails printRectangleDetails} which,
 * when given the height and width of a rectangle, prints four lines of output stating the height, width, area, and
 * perimeter of the rectangle. The output should include suitable text explaining what the numbers area. Add suitable
 * comments to the new method.
 *
 * <br />
 * <br />
 *
 * {@link Rectangle#printRectangleDetails printRectangleDetails()} is a different kind of method from
 * {@link Rectangle#area area} and {@link Rectangle#perimeter perimeter}. It performs some actions (printing output)
 * but does not return any result when it is called. Its output type must therefore be {@code void} to show that it
 * does not return a value, and it is an example of a <i>void method</i>.
 *
 * <br />
 * <br />
 *
 * A call to a void method can be used as a standalone statement in your program. However, because a void method does
 * not return a value, it cannot be used where an expression with a value is expected. Methods which return a value
 * (non-void methods) can be used as both standalone statements and as expressions. Here are some examples:
 *
 * <br />
 * <br />
 * These lines of code are correct:
 *
 * <pre>
 * int x = area(5, 3);          // Non-void method call used as expression
 * area(5, 3);                  // Non-void method call used as statement
 * printRectangleDetails(5, 3); // Void method call used as statement
 * </pre>
 *
 * This code is incorrect because it attempts to use a void method call as an expression:
 *
 * <pre>
 * int x = printRectangleDetails(5, 3);
 * </pre>
 *
 * Replace the code in the {@link Rectangle#main main} method with suitable calls of your
 * {@link Rectangle#printRectangleDetails printRectangleDetails()} method.
 *
 * @author Michael Sammels
 * @version 15.10.2018
 * @since 1.0
 */
package methods.methods1;
