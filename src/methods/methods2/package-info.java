/**
 * <p>
 * Methods: Part 2
 * </p>
 *
 * In this part of the practical you will continue developing the {@link methods.methods1.Rectangle Rectangle} program
 * to make it more user-friendly and versatile, while keeping the code well structured through the use of methods.
 *
 * <br />
 * <br />
 *
 * Do this work on a copy of the <b>{@link methods.methods1.Rectangle Rectangle}</b> project (call it
 * <b>{@link InteractiveRectangle#InteractiveRectangle InteractiveRectangle}</b>) so that you do not overwrite the work
 * you did for the previous practical.
 *
 * <br />
 * <br />
 *
 * The first change you are going to make is to get the program to ask the user to provide the height and width of the
 * rectangle. You saw examples of how to read {@link java.lang.Integer integers} into your program in Practicals 7 and
 * 8. This time we are going to do things a little differently. Our program needs to read two {@link java.lang.Integer
 * integers}. Instead of writing the same code twice, we will write a method for reading {@link java.lang.Integer
 * integers} and call that method twice.
 *
 * <br />
 * <br />
 *
 * Our program will need to import the {@link java.util.Scanner Scanner} library in order to read from the terminal.
 * Add the usual {@code import} statement at the top of the program:
 *
 * <pre>
 * import java.util.Scanner;
 * </pre>
 *
 * The next step is a small challenge. Complete the body of the method below. This method takes as its parameter a
 * {@link java.lang.String string} which is the prompt to be shown to the user. The method displays this prompt and
 * then attempts to read a positive {@link java.lang.Integer integer}. The method discards any non-integer values that
 * the user might enter, displaying a suitable error message and repeating the prompt. When the user enters an
 * {@link java.lang.Integer integer} it is read and its value is returned.
 *
 * <pre>
 * {@literal /**}
 *  * Read in an integer and return its value.
 *  * {@literal @param} the prompt to be shown to the user.
 *  *&#47;
 * public static int readInteger(String prompt) {
 *     // Add your code here
 * }
 * </pre>
 *
 * [Hint: You will find it useful to reuse code from the <b>{@link loops.loopsA.loopsA1.InputLoop InputLoop}</b>
 * program from Practical 7, making any changes that are necessary.]
 *
 * <br />
 * <br />
 *
 * Change the {@link InteractiveRectangle#main main} method so that it calls {@link InteractiveRectangle#readInteger
 * readInteger()} twice with suitable arguments, storing the values returned in suitably named variables, and then
 * calling printRectangleDetails() with these variables as arguments.
 *
 * <br />
 * <br />
 *
 * Now comes another moderately challenging step. Your program currently allows the user to enter negative or zero
 * values for the height and width of the rectangle. These make no sense so your next step is to disallow them.
 * Complete the code below to create a method called {@link InteractiveRectangle#readPositiveInteger
 * readPositiveInteger()} which reads in and returns an {@link java.lang.Integer integer} greater than 0. If the user
 * types in a negative number or 0, this method must display an error message and request the input again.
 *
 * <pre>
 * {@literal /**}
 *  * Read in an integer greater than 0 and return its value.
 *  * {@literal @param} the prompt to be shown to the user.
 *  *&#47;
 * public static int readPositiveInteger(String prompt) {
 *     // Add your code here
 * }
 * </pre>
 *
 * [Hint: Use a {@link java.lang.Boolean Boolean} variable to control a loop which stops when a positive
 * {@link java.lang.Integer integer} has been read successfully. Initialise this {@link java.lang.Boolean Boolean}
 * variable to {@code false}. Inside the loop there should be a call to {@link InteractiveRectangle#readInteger
 * readInteger()} followed by an {@code if} statement which examines the value returned from that call, displaying an
 * error message if the value is not positive, and updating the {@link java.lang.Boolean Boolean} variable to
 * {@code true} if the value is positive. Include a return statement at the end of the method to return the input when
 * it has been successfully read.]
 *
 * <br />
 * <br />
 *
 * Replace the calls to {@link InteractiveRectangle#readInteger readInteger()} in the {@link InteractiveRectangle#main
 * main} method with calls to {@link InteractiveRectangle#readPositiveInteger readPositiveInteger()}. Test your program
 * by running with a suitable selection of inputs.
 *
 * <br />
 * <br />
 *
 * You probably found it rather tedious to run your program separately for each test in the previous step. To complete
 * this program, we are going to make it ask if the user would like to process another rectangle, and keep going until
 * the user answers this question with “n”. You may remember doing something similar in the
 * <b>{@link loops.loopsB.loopsB1.DrawTriangle DrawTriangle}</b> project in Practical 8. The code in that project
 * became rather complex because we were doing everything in the {@link loops.loopsB.loopsB1.DrawTriangle#main main}
 * method. This time we will keep the {@link InteractiveRectangle#main main} method nice and simple by partitioning off
 * the code for consulting the user into a separate method.
 *
 * <br />
 * <br />
 *
 * Add the code below to your program. This code is a <i>stub</i> (an incomplete skeleton) for a method called
 * {@link InteractiveRectangle#keepGoing keepGoing()} which will check if the user wishes to continue or not. The
 * method takes no parameters and returns a {@link java.lang.Boolean Boolean} result. Do not add code to the method
 * body yet.
 *
 * <pre>
 * {@literal /**}
 *  * Ask the user whether or not to continue
 *  * and returns the result as a boolean.
 *  *&#47;
 * public static boolean keepGoing() {
 *     // Code to be added later
 * }
 * </pre>
 *
 * The {@link InteractiveRectangle#main main} method will use the {@link java.lang.Boolean Boolean} value returned by
 * {@link InteractiveRectangle#keepGoing keepGoing()} to decide whether to continue processing rectangles or to quit. A
 * {@code do-while} loop gives us an elegant way to express this logic. A goodbye message before the program stops is a
 * nice finishing touch.
 *
 * <br />
 * <br />
 *
 * Change the {@link InteractiveRectangle#main main} method by adding a {@code do-while} loop and a goodbye message as
 * indicated below:
 *
 * <pre>
 * do {
 *     // Existing code goes here
 * } while (keepGoing());
 * System.out.println("[A goodbye message of your choice]");
 * </pre>
 *
 * It is now time to complete the {@link InteractiveRectangle#keepGoing keepGoing()} method. This method must show the
 * user a suitable prompt, such as, “Would you like to process another rectangle (y/n)?” and then read a
 * {@link java.lang.String string} typed in by the user. If the {@link java.lang.String string} is “y”, the method
 * returns {@code true}. If the {@link java.lang.String string} is “n”, the method returns {@code false}. If the user
 * responds with anything other than “y” or “n” the response is ignored and the prompt is repeated.
 *
 * <br />
 * <br />
 *
 * This method combines aspects of the {@link InteractiveRectangle#readInteger readInteger()} and
 * {@link InteractiveRectangle#readPositiveInteger readPositiveInteger()} methods. The pseudocode below will help you
 * to implement it:
 *
 * <pre>
 * Declare a Scanner object for reading input
 * Declare Boolean variables result and inputRead
 * Set inputRead to false
 *
 * While inputRead is false
 *     Ask if user wants to process another rectangle
 *     Read input string
 *
 *     If input is “y” then
 *         Set inputRead to true
 *         Set result to true
 *     Else
 *         If input is “n” then
 *         Set inputRead to true
 *         Set result to false
 * Return result
 * </pre>
 *
 * @author Michael Sammels
 * @version 15.10.2018
 * @since 1.0
 */
package methods.methods2;
