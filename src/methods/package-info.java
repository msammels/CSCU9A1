/**
 * <p>
 * <b>Practical 9: Methods</b>
 * </p>
 * <p>
 * <b>Aims:</b>
 * </p>
 *
 * <ul>
 * <li>To learn about methods, parameters, arguments and return values.</li>
 * <li>To learn about methods without return values ({@code void}) methods).</li>
 * <li>To create methods that can be reused for multiple purposes.</li>
 * </ul>
 *
 * @author Michael Sammels
 * @version 15.10.2018
 * @since 1.0
 */
package methods;
